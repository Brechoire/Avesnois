**Mon Avesnois**


Mon Avesnois un annuaire de professionnels.

Mon Avesnois a pour but de répertorier les professionnels de l'Avesnois dans un esprit différent des annuaires habituels et ainsi proposer une mise en avant des services ou produits d’une façon originale. 

Install : 
composer install
php bin/console ckeditor:install
php bin/console assets:install public