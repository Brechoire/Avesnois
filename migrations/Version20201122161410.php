<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201122161410 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE city (id INT AUTO_INCREMENT NOT NULL, city VARCHAR(255) NOT NULL, postal VARCHAR(255) NOT NULL, FULLTEXT INDEX IDX_2D5B02342D5B0234 (city), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE connection_history (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, ip VARCHAR(255) NOT NULL, last_login DATETIME NOT NULL, host_name VARCHAR(255) NOT NULL, INDEX IDX_5CB09668A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE image_ban (id INT AUTO_INCREMENT NOT NULL, page_id INT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_3A1400B6C4663E4 (page_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE image_logo (id INT AUTO_INCREMENT NOT NULL, page_id INT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_16B4708DC4663E4 (page_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE naf_code (id INT AUTO_INCREMENT NOT NULL, naf_code VARCHAR(255) NOT NULL, titled_naf VARCHAR(255) NOT NULL, titled_naf_65 VARCHAR(255) NOT NULL, titled_naf_40 VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE page (id INT AUTO_INCREMENT NOT NULL, city_id INT NOT NULL, naf_code_id INT NOT NULL, user_id INT NOT NULL, name VARCHAR(90) NOT NULL, description LONGTEXT NOT NULL, siret VARCHAR(14) NOT NULL, address VARCHAR(255) NOT NULL, tel1 VARCHAR(10) NOT NULL, tel2 VARCHAR(10) DEFAULT NULL, email1 VARCHAR(80) NOT NULL, email2 VARCHAR(80) DEFAULT NULL, website VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, valide_admin TINYINT(1) NOT NULL, slug VARCHAR(128) NOT NULL, is_activaded TINYINT(1) NOT NULL, is_published TINYINT(1) NOT NULL, address_published TINYINT(1) NOT NULL, tel1_published TINYINT(1) NOT NULL, siret_published TINYINT(1) NOT NULL, email1_published TINYINT(1) NOT NULL, fax VARCHAR(10) DEFAULT NULL, schedule_published TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_140AB620989D9B62 (slug), INDEX IDX_140AB6208BAC62AF (city_id), INDEX IDX_140AB6204EC95BB1 (naf_code_id), UNIQUE INDEX UNIQ_140AB620A76ED395 (user_id), FULLTEXT INDEX IDX_140AB6205E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE schedule (id INT AUTO_INCREMENT NOT NULL, page_id INT DEFAULT NULL, mon_pm TIME DEFAULT NULL, mon_am TIME DEFAULT NULL, tue_pm TIME DEFAULT NULL, tue_am TIME DEFAULT NULL, wed_pm TIME DEFAULT NULL, wed_am TIME DEFAULT NULL, thu_pm TIME DEFAULT NULL, thu_am TIME DEFAULT NULL, fri_pm TIME DEFAULT NULL, fri_am TIME DEFAULT NULL, sat_pm TIME DEFAULT NULL, sat_am TIME DEFAULT NULL, sun_pm TIME DEFAULT NULL, sun_am TIME DEFAULT NULL, mon_pm_end TIME DEFAULT NULL, mon_am_end TIME DEFAULT NULL, tue_pm_end TIME DEFAULT NULL, tue_am_end TIME DEFAULT NULL, wed_pm_end TIME DEFAULT NULL, wed_am_end TIME DEFAULT NULL, thu_pm_end TIME DEFAULT NULL, thu_am_end TIME DEFAULT NULL, fri_pm_end TIME DEFAULT NULL, fri_am_end TIME DEFAULT NULL, sat_pm_end TIME DEFAULT NULL, sat_am_end TIME DEFAULT NULL, sun_pm_end TIME DEFAULT NULL, sun_am_end TIME DEFAULT NULL, close_mon VARCHAR(255) DEFAULT NULL, close_tue VARCHAR(255) DEFAULT NULL, close_wed VARCHAR(255) DEFAULT NULL, close_thu VARCHAR(255) DEFAULT NULL, close_fri VARCHAR(255) DEFAULT NULL, close_sat VARCHAR(255) DEFAULT NULL, close_sun VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_5A3811FBC4663E4 (page_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE service (id INT AUTO_INCREMENT NOT NULL, page_id INT DEFAULT NULL, title VARCHAR(100) NOT NULL, content VARCHAR(150) NOT NULL, img_service VARCHAR(255) NOT NULL, content_back VARCHAR(150) DEFAULT NULL, description LONGTEXT DEFAULT NULL, color_back VARCHAR(255) DEFAULT NULL, tag VARCHAR(255) NOT NULL, INDEX IDX_E19D9AD2C4663E4 (page_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE social (id INT AUTO_INCREMENT NOT NULL, page_id INT DEFAULT NULL, facebook VARCHAR(255) DEFAULT NULL, linkedin VARCHAR(255) DEFAULT NULL, youtube VARCHAR(255) DEFAULT NULL, instagram VARCHAR(255) DEFAULT NULL, twitter VARCHAR(255) DEFAULT NULL, viadeo VARCHAR(255) DEFAULT NULL, github VARCHAR(255) DEFAULT NULL, tumblr VARCHAR(255) DEFAULT NULL, pinterest VARCHAR(255) DEFAULT NULL, flickr VARCHAR(255) DEFAULT NULL, twitch VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_7161E187C4663E4 (page_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, city_id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, username VARCHAR(60) NOT NULL, created_at DATETIME NOT NULL, active TINYINT(1) NOT NULL, type_user TINYINT(1) NOT NULL, created_ip VARCHAR(255) NOT NULL, last_connection DATETIME DEFAULT NULL, last_ip VARCHAR(255) NOT NULL, host_name VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, token VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), INDEX IDX_8D93D6498BAC62AF (city_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE connection_history ADD CONSTRAINT FK_5CB09668A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE image_ban ADD CONSTRAINT FK_3A1400B6C4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('ALTER TABLE image_logo ADD CONSTRAINT FK_16B4708DC4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB6208BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB6204EC95BB1 FOREIGN KEY (naf_code_id) REFERENCES naf_code (id)');
        $this->addSql('ALTER TABLE page ADD CONSTRAINT FK_140AB620A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE schedule ADD CONSTRAINT FK_5A3811FBC4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('ALTER TABLE service ADD CONSTRAINT FK_E19D9AD2C4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('ALTER TABLE social ADD CONSTRAINT FK_7161E187C4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6498BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE page DROP FOREIGN KEY FK_140AB6208BAC62AF');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6498BAC62AF');
        $this->addSql('ALTER TABLE page DROP FOREIGN KEY FK_140AB6204EC95BB1');
        $this->addSql('ALTER TABLE image_ban DROP FOREIGN KEY FK_3A1400B6C4663E4');
        $this->addSql('ALTER TABLE image_logo DROP FOREIGN KEY FK_16B4708DC4663E4');
        $this->addSql('ALTER TABLE schedule DROP FOREIGN KEY FK_5A3811FBC4663E4');
        $this->addSql('ALTER TABLE service DROP FOREIGN KEY FK_E19D9AD2C4663E4');
        $this->addSql('ALTER TABLE social DROP FOREIGN KEY FK_7161E187C4663E4');
        $this->addSql('ALTER TABLE connection_history DROP FOREIGN KEY FK_5CB09668A76ED395');
        $this->addSql('ALTER TABLE page DROP FOREIGN KEY FK_140AB620A76ED395');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE connection_history');
        $this->addSql('DROP TABLE image_ban');
        $this->addSql('DROP TABLE image_logo');
        $this->addSql('DROP TABLE naf_code');
        $this->addSql('DROP TABLE page');
        $this->addSql('DROP TABLE schedule');
        $this->addSql('DROP TABLE service');
        $this->addSql('DROP TABLE social');
        $this->addSql('DROP TABLE user');
    }
}
