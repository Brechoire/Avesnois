<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210111163245 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE contact_pro ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contact_pro ADD CONSTRAINT FK_968EB7C7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_968EB7C7A76ED395 ON contact_pro (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE contact_pro DROP FOREIGN KEY FK_968EB7C7A76ED395');
        $this->addSql('DROP INDEX IDX_968EB7C7A76ED395 ON contact_pro');
        $this->addSql('ALTER TABLE contact_pro DROP user_id');
    }
}
