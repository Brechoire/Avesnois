// Horaires d'ouverture semaine
$(document).ready(function(){
     /**
     *
     * @param close_day | Jour de fermeture
     * @param class_day_pm | Classe du jour matin
     * @param class_day_am | Classe du jour après midi
     * @param day_pm | Fermture matin
     * @param day_am | Fermture après midi
     * @param day_day | Fermture journée
     */
    function closeTime(close_day, class_day_pm, class_day_am, day_pm, day_am, day_day) {
        document.getElementById(close_day).onchange = function() { timeClose() };
        let timeVal = document.getElementById(close_day);
        let timeValMClass = document.querySelectorAll(class_day_pm);
        let timeValAMClass = document.querySelectorAll(class_day_am);
        function timeClose() {


            if (timeVal.value === day_pm) {

                timeValMClass.forEach(element => {
                    element.setAttribute("disabled", "true");
                });

                timeValAMClass.forEach(element => {
                    element.removeAttribute("disabled");
                });

            }else if (timeVal.value === day_am){

                timeValMClass.forEach(element => {
                    element.removeAttribute("disabled");
                });

                timeValAMClass.forEach(element => {
                    element.setAttribute("disabled", "true");
                });

            }else if (timeVal.value === day_day){

                timeValMClass.forEach(element => {
                    element.setAttribute("disabled", "true");
                });

                timeValAMClass.forEach(element => {
                    element.setAttribute("disabled", "true");
                });

            }else {

                timeValMClass.forEach(element => {
                    element.removeAttribute("disabled");
                });

                timeValAMClass.forEach(element => {
                    element.removeAttribute("disabled");
                });
            }
        }

        if (timeVal.value === day_pm) {

            timeValMClass.forEach(element => {
                element.setAttribute("disabled", "true");
            });

            timeValAMClass.forEach(element => {
                element.removeAttribute("disabled");
            });

        }else if (timeVal.value === day_am){

            timeValMClass.forEach(element => {
                element.removeAttribute("disabled");
            });

            timeValAMClass.forEach(element => {
                element.setAttribute("disabled", "true");
            });

        }else if (timeVal.value === day_day){

            timeValMClass.forEach(element => {
                element.setAttribute("disabled", "true");
            });

            timeValAMClass.forEach(element => {
                element.setAttribute("disabled", "true");
            });

        }else {

            timeValMClass.forEach(element => {
                element.removeAttribute("disabled");
            });

            timeValAMClass.forEach(element => {
                element.removeAttribute("disabled");
            });
        }
    }

    closeTime("schedule_close_mon", ".mon_pm", ".mon_am", "close_pm", "close_am", "close_day");
    closeTime("schedule_close_tue", ".tue_pm", ".tue_am", "close_pm", "close_am", "close_day");
    closeTime("schedule_close_wed", ".wed_pm", ".wed_am", "close_pm", "close_am", "close_day");
    closeTime("schedule_close_thu", ".thu_pm", ".thu_am", "close_pm", "close_am", "close_day");
    closeTime("schedule_close_fri", ".fri_pm", ".fri_am", "close_pm", "close_am", "close_day");
    closeTime("schedule_close_sat", ".sat_pm", ".sat_am", "close_pm", "close_am", "close_day");
    closeTime("schedule_close_sun", ".sun_pm", ".sun_am", "close_pm", "close_am", "close_day");

    function borderError(hoursM, hoursAM){

        $(hoursM).on('keyup mouseup mousedown mouseout', function(event){

            let myClass = $(this).val();

            if (myClass === ""){

                $(this).css("border-color", "red");

            }else {

                $(this).css("border-color", "")

            }

        });

        $(hoursAM).on('keyup mouseup mousedown mouseout', function(event){

            let myClass = $(this).val();

            if (myClass === ""){

                $(this).css("border-color", "red");

            }else {

                $(this).css("border-color", "")

            }

        });

    }
    borderError('.mon_pm', '.mon_am');
    borderError('.tue_pm', '.tue_am');
    borderError('.wed_pm', '.wed_am');
    borderError('.thu_pm', '.thu_am');
    borderError('.fri_pm', '.fri_am');
    borderError('.sat_pm', '.sat_am');
    borderError('.sun_pm', '.sun_am');

});