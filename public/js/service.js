function imgService(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function (e) {
            $('#imgService').attr('style', 'background-image: url(' +  e.target.result + ')');
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function titleService() {
    let title = document.getElementById("service_title").value;
    document.getElementById("title").innerHTML = title;
    document.getElementById("modal-title").innerHTML = title;
}

function contentService() {
    let x = document.getElementById("service_content").value;
    document.getElementById("content-front").innerHTML = x;
}

function contentModalService() {
    let x = document.getElementsByClassName("cke_1").value;
    document.getElementById("service-content").innerHTML = x;
    console.log(x);
}

function contentServiceBack() {
    let x = document.getElementById("service_contentBack").value;
    document.getElementById("content-back").innerHTML = x;
}

let colorPicker = new iro.ColorPicker("#picker", {
    width: 150,
    color: "#f00"
});

colorPicker.on('color:change', function(color) {
    document.getElementById("service_colorBack").value = color.hexString;
    $('#color-back').attr('style', 'background: ' +  color.hexString );
});