<?php

namespace App\Controller;

use App\Service\SearchService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 * @package App\Controller
 *
 * Page d'accueil
 */
class HomeController extends AbstractController
{
    /**
     * @param SearchService $searchService
     * @param Request $request
     * @return Response
     * @Route("/", name="home", schemes={"https"}))
     */
    public function index(SearchService $searchService, Request $request): Response
    {
        $form = $searchService->search($request);

        /**
         * Renvois les résultats de la recherche effectué sur la page d'accueil
         */
        if ($form['form']->isSubmitted() && $form['form']->isValid()) {
            return $this->forward('App\Controller\SearchController::search');
        }

        return $this->render("home.html.twig", [
            'form' => $form['form']->createView()
        ]);
    }
}
