<?php

namespace App\Controller\Import;

use App\Entity\City;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Importation des villes via une route
 * Class ImportCity
 * @package App\Controller\Import
 */
class ImportCity extends AbstractController
{
    /**
     * @return Response
     * @Route("/city", name="city", schemes={"https"})
     */
    public function import(): Response
    {

        $cities = array();
        $row = 0;

        if (($handle = fopen(__DIR__ . "/../../../public/Imports/city.csv", "r")) !== false) {
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                $num = count($data);
                $row++;
                for ($c = 0; $c < $num; $c++) {
                    $cities[$row] = array(
                        "city" => $data[0],
                        "postal" => $data[1],
                    );
                }
            }
            fclose($handle);
        }

        $em = $this->getDoctrine()->getManager();

        foreach ($cities as $ci) {
            $city = new City();
            $city->setCity($ci["city"]);
            $city->setPostal($ci["postal"]);
            $em->persist($city);
        }

        $em->flush();

        return new Response('Importation des villes dans la base de données ok');
    }
}
