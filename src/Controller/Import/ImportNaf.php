<?php

namespace App\Controller\Import;

use App\Entity\NafCode;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Importation des villes via une route
 * Class ImportCity
 * @package App\Controller\Import
 */
class ImportNaf extends AbstractController
{
    /**
     * @return Response
     * @Route("/naf", name="naf", schemes={"https"})
     */
    public function import(): Response
    {

        $codeNaf = array();
        $row = 0;

        if (($handle = fopen(__DIR__ . "/../../../public/Imports/naf.csv", "r")) !== false) {
            while (($data = fgetcsv($handle, 2000, ";")) !== false) {
                $num = count($data);
                $row++;
                for ($c = 0; $c < $num; $c++) {
                    $codeNaf[$row] = array(
                        "naf_code" => $data[0],
                        "titled_naf" => $data[1],
                        "titled_naf_40" => $data[2],
                        "titled_naf_65" => $data[3],
                    );
                }
            }
            fclose($handle);
        }

        $em = $this->getDoctrine()->getManager();

        foreach ($codeNaf as $ci) {
            $naf = new NafCode();
            $naf->setNafCode($ci["naf_code"]);
            $naf->setTitledNaf($ci["titled_naf"]);
            $naf->setTitledNaf40($ci["titled_naf_40"]);
            $naf->setTitledNaf65($ci["titled_naf_65"]);
            $em->persist($naf);
        }

        $em->flush();

        return new Response('Importation des codes naf dans la base de données ok');
    }
}
