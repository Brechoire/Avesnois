<?php

namespace App\Controller\Page;

use App\Service\PageProService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AddServiceController
 * @package App\Controller\Page
 * @Route("/profil/mapage")
 */
class AddServiceController extends AbstractController
{
    /**
     * @Route("/ajout-service", name="add_service_page_pro", schemes={"https"})
     * @param Request $request
     * @param PageProService $pageProService
     * @return Response
     */
    public function index(Request $request, PageProService $pageProService): Response
    {
        $id = $pageProService->getIdUser();
        $this->denyAccessUnlessGranted('ROLE_PRO');

        $check = $pageProService->PagePro($id);
        if ($check == null) {
            return $this->redirectToRoute('profile', ['id' => $id]);
        }

        /**
         * Redirects the user if he has all his services
         */
        $countService = $pageProService->countServiceUser($check->getId());
        if ($countService >= 5) {
            $this->addFlash('errors', 'Impossible d\'ajouter plus de service');
            return $this->redirectToRoute('profile', ['id' => $id]);
        }

        $form = $pageProService->addService($request, $check);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Service ajouté');

            return $this->redirectToRoute('list_service_page_pro', ['id' => $id]);
        }

        return $this->render('pagepro/add_service_page_pro.html.twig', [
            'form' => $form->createView(),
            'check' => $check
        ]);
    }
}
