<?php

namespace App\Controller\Page;

use App\Exception\RedirectException;
use App\Service\PageProService;
use App\Service\ProfileService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ConnectionHistoryController
 * @package App\Controller\Profile
 * @Route("/profil/pro")
 */
class ConnectionHistoryController extends AbstractController
{
    /**
     * @Route("/{id}/historique-connexion", name="connection_history_pro", schemes={"https"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param $id
     * @param ProfileService $profileService
     * @param PageProService $pageProService
     * @return RedirectResponse|Response
     * @throws RedirectException
     */
    public function index(
        Request $request,
        $id,
        ProfileService $profileService,
        PageProService $pageProService
    ) {
        $profileService->checkGetIdUser($profileService->getIdUser(), $request->get('id'));

        $connection_history = $profileService->connectionHistoryPro($id);

        $check = $pageProService->PagePro($id);

        return $this->render('profile/connection_history_pro.html.twig', [
            'connection_history' => $connection_history,
            'check' => $check
        ]);
    }
}
