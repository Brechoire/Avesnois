<?php

namespace App\Controller\Page;

use App\Exception\RedirectException;
use App\Service\PageProService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ContactController
 * @package App\Controller\Page
 */
class ContactController extends AbstractController
{
    /**
     * @Route("/page/{slug}/contact", name="contact_page")
     * @param $slug
     * @param PageProService $pageProService
     * @param Request $request
     * @return Response
     * @throws RedirectException
     */
    public function index($slug, PageProService $pageProService, Request $request): Response
    {
        $page = $pageProService->showPagePro($slug);
        $pageProService->ExistPage($page);

        $pageProService->redirectPage($page->getIsPublished(), $page->getIsActivaded());

        $form = $pageProService->contact($request, $page);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Message envoyé');
            return $this->redirectToRoute('page_pro', ['slug' => $page->getSlug()]);
        }
        return $this->render('contact.html.twig', [
            'page' => $page,
            'form' => $form->createView()
        ]);
    }
}
