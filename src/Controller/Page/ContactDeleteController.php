<?php

namespace App\Controller\Page;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactDeleteController extends AbstractController
{
    /**
     * @Route("/message/delete", name="delete_message_pro", methods={"GET"})
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function index(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            return $this->json($request->request->all());
        } else {
            return new JsonResponse([
                'type' => 'error',
                'message' => 'Ajax only'
            ]);
        }
    }
}
