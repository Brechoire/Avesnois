<?php

namespace App\Controller\Page;

use App\Entity\Service;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class DeleteServiceController
 * @package App\Controller\Page
 * @Route("/profil/mapage")
 */
class DeleteServiceController extends AbstractController
{
    /**
     * @Route("/service/supprimer/{id}/{token}", name="delete_service")
     * @param Service $service
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface $tokenStorage
     * @param Request $request
     * @return Response
     */
    public function index(
        Service $service,
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        Request $request
    ): Response {
        $submittedToken = $request->get('token');

        if ($this->isCsrfTokenValid('delete-service', $submittedToken)) {
            $em->remove($service);
            $em->flush();
            $this->addFlash('success', 'Service/produit bien supprimé');
        }

        return $this->redirectToRoute('list_service_page_pro', ['id' => $tokenStorage->getToken()->getUser()->getId()]);
    }
}
