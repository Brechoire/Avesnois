<?php

namespace App\Controller\Page;

use App\Repository\ImageBanRepository;
use App\Repository\ImageLogoRepository;
use App\Service\PageProService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Edition de la page PRO
 * @package App\Controller\Page
 * @Route("/profil/mapage")
 */
class EditPageProController extends AbstractController
{
    /**
     * @Route("/{id}/edition/{token}", name="edit_page_pro", schemes={"https"})
     * @param Request $request
     * @param PageProService $pageProService
     * @param int $id | Id de l'utilisateur
     * @param ImageLogoRepository $imageLogoRepository
     * @param ImageBanRepository $imageBanRepository
     * @return Response
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function index(
        Request $request,
        PageProService $pageProService,
        int $id,
        ImageLogoRepository $imageLogoRepository,
        ImageBanRepository $imageBanRepository
    ): Response {
        $idUser = $pageProService->getIdUser();

        if ($id != $idUser) {
            return $this->redirectToRoute('profile', ['id' => $idUser]);
        }

        /**
         * Check Token Edit
         */
        $submittedToken = $request->get('token');
        if (!$this->isCsrfTokenValid('edit_page_pro', $submittedToken)) {
            return $this->redirectToRoute('home');
        }

        $this->denyAccessUnlessGranted('ROLE_PRO');

        $check = $pageProService->PagePro($id);

        if ($check == null) {
            return $this->redirectToRoute('profile', ['id' => $id]);
        }

        $form = $pageProService->editPage($request, $id);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Vous avez modifié vos informations');
            return $this->redirectToRoute('edit_page_pro', ['id' => $id, 'token' => $submittedToken]);
        }

        $logo = $imageLogoRepository->imageLogoByPage($check->getId());

        $ban = $imageBanRepository->imageBanByPage($check->getId());

        return $this->render('pagepro/edit_page_pro.html.twig', [
            'form' => $form->createView(),
            'check' => $check,
            'logo' => $logo,
            'ban' => $ban,
        ]);
    }
}
