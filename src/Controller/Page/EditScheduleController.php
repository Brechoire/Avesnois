<?php

namespace App\Controller\Page;

use App\Service\PageProService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Edition des horaires
 * @package App\Controller\Page
 * @Route("/profil/mapage")
 */
class EditScheduleController extends AbstractController
{
    /**
     * @param Request $request
     * @param PageProService $pageProService
     * @return Response
     * @Route("/{id}/edit/horaires/{token}", name="edit_schedule", schemes={"https"})
     */
    public function index(Request $request, PageProService $pageProService): Response
    {
        $id = $pageProService->getIdUser();

        if ($id != $request->get('id')) {
            return $this->redirectToRoute('profile', ['id' => $id]);
        }

        $this->denyAccessUnlessGranted('ROLE_PRO');

        /**
         * Check Token Edit
         */
        $submittedToken = $request->get('token');
        if (!$this->isCsrfTokenValid('edit_schedule', $submittedToken)) {
            return $this->redirectToRoute('home');
        }

        $page_pro = $pageProService->PagePro($id);

        if ($page_pro == null) {
            return $this->redirectToRoute('profile', ['id' => $id]);
        }

        $form = $pageProService->schedule($request, $page_pro->getId());

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Vous avez modifié vos horaires');
            return $this->redirectToRoute('edit_schedule', ['id' => $id, 'token' => $submittedToken]);
        }

        return $this->render('pagepro/edit_schedule.html.twig', [
            'check' => $page_pro,
            'form' => $form->createView()
        ]);
    }
}
