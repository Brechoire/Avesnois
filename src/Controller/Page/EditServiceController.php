<?php

namespace App\Controller\Page;

use App\Service\PageProService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AddServiceController
 * @package App\Controller\Page
 * @Route("/profil/mapage")
 */
class EditServiceController extends AbstractController
{
    /**
     * @Route("/edit-service/{id}/{token}", name="edit_service_page_pro", schemes={"https"})
     * @param Request $request
     * @param PageProService $pageProService
     * @param $id
     * @return Response
     */
    public function index(Request $request, PageProService $pageProService, $id): Response
    {
        $idUser = $pageProService->getIdUser();
        $this->denyAccessUnlessGranted('ROLE_PRO');

        /**
         * Checks if the user has a page
         * Checks if the user can modify the service.
         */
        $check = $pageProService->PagePro($idUser);
        $checkService = $pageProService->checkService($id, $check->getId());
        if ($check == null || $checkService == null) {
            return $this->redirectToRoute('profile', ['id' => $idUser]);
        }

        /**
         * Check Token Edit
         */
        $submittedToken = $request->get('token');
        if (!$this->isCsrfTokenValid('edit_service', $submittedToken)) {
            return $this->redirectToRoute('home');
        }

        $form = $pageProService->editService($request, $id, $check->getId());

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Votre modification à bien été effectué');

            return $this->redirectToRoute('edit_service_page_pro', ['id' => $id, 'token' => $submittedToken]);
        }

        return $this->render('pagepro/edit_service_page_pro.html.twig', [
            'form' => $form->createView(),
            'check' => $check,
            'services' => $checkService
        ]);
    }
}
