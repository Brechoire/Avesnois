<?php

namespace App\Controller\Page;

use App\Service\PageProService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Edition des liens sociaux page PRO
 * @package App\Controller\Page
 * @Route("/profil/mapage")
 */
class EditSocialNetworksPageProController extends AbstractController
{
    /**
     * @Route("/{id}/edit/reseauxsociaux/{token}", name="edit_social_networks_page_pro", schemes={"https"})
     * @param Request $request
     * @param PageProService $pageProService
     * @return Response
     */
    public function index(Request $request, PageProService $pageProService): Response
    {
        /**
         * Check Token Edit
         */
        $submittedToken = $request->get('token');
        if (!$this->isCsrfTokenValid('edit_social_networks_page_pro', $submittedToken)) {
            return $this->redirectToRoute('home');
        }

        $id = $pageProService->getIdUser();

        if ($id != $request->get('id')) {
            return $this->redirectToRoute('profile', ['id' => $id]);
        }

        $this->denyAccessUnlessGranted('ROLE_PRO');

        $page_pro = $pageProService->PagePro($id);

        if ($page_pro == null) {
            return $this->redirectToRoute('profile', ['id' => $id]);
        }

        $form = $pageProService->editPageSocialNetworks($request, $page_pro->getId());

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Vous avez modifié vos réseaux sociaux');
            return $this->redirectToRoute('edit_social_networks_page_pro', ['id' => $id, 'token' => $submittedToken]);
        }

        return $this->render('pagepro/edit_social_networks_page_pro.html.twig', [
            'form' => $form->createView(),
            'check' => $page_pro
        ]);
    }
}
