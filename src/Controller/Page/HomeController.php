<?php

namespace App\Controller\Page;

use App\Exception\RedirectException;
use App\Service\PageProService;
use App\Service\ProfileService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profil")
 * Class HomeController
 * @package App\Controller\Page
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/pro/{id}", name="pro_profile", schemes={"https"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param int $id
     * @param ProfileService $profileService
     * @param PageProService $pageProService
     * @return RedirectResponse|Response
     * @throws RedirectException
     */
    public function index(
        Request $request,
        int $id,
        ProfileService $profileService,
        PageProService $pageProService
    ) {
        $profileService->checkGetIdUser($profileService->getIdUser(), $request->get('id'));

        $connection_history = $profileService->connectionHistory($id);

        $check = $pageProService->PagePro($id);

        return $this->render('pagepro/index.html.twig', [
            'connection_history' => $connection_history,
            'check' => $check,
        ]);
    }
}
