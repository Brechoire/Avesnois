<?php

namespace App\Controller\Page;

use App\Service\PageProService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Accueil profil page PRO
 * @package App\Controller\Page
 * @Route("/profil/mapage")
 */
class HomeProfilePageProController extends AbstractController
{
    /**
     * @Route("/{id}", name="home_pro_profile", schemes={"https"}, requirements={"id"="\d+"})
     * @param $id
     * @param PageProService $pageProService
     * @return Response
     */
    public function index($id, PageProService $pageProService): Response
    {
        $idUser = $pageProService->getIdUser();

        if ($id != $idUser) {
            return $this->redirectToRoute('profile', ['id' => $idUser]);
        }

        $this->denyAccessUnlessGranted('ROLE_PRO');
        $page = $pageProService->PagePro($idUser);

        if ($page == null) {
            return $this->redirectToRoute('profile', ['id' => $id]);
        }

        $countService = $pageProService->countServiceUser($page->getId());

        return $this->render('pagepro/profile_pro_page.html.twig', [
            'page' => $page,
            'check' => $page,
            'countService' => $countService
        ]);
    }
}
