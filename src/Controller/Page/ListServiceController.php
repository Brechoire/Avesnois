<?php

namespace App\Controller\Page;

use App\Service\PageProService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ListServiceController
 * @package App\Controller\Page
 * @Route("/profil/mapage")
 */
class ListServiceController extends AbstractController
{
    /**
     * @Route("/{id}/liste-service", name="list_service_page_pro", schemes={"https"})
     * @param Request $request
     * @param PageProService $pageProService
     * @return Response
     */
    public function index(Request $request, PageProService $pageProService): Response
    {
        $idUser = $pageProService->getIdUser();

        if ($idUser != $request->get('id')) {
            return $this->redirectToRoute('profile', ['id' => $idUser]);
        }
        $this->denyAccessUnlessGranted('ROLE_PRO');

        $check = $pageProService->PagePro($idUser);

        if ($check == null) {
            return $this->redirectToRoute('profile', ['id' => $idUser]);
        }

        $service = $pageProService->listService($check->getId());

        $countService = $pageProService->countServiceUser($check->getId());

        return $this->render('pagepro/list_service.html.twig', [
            'check' => $check,
            'services' => $service,
            'countService' => $countService
        ]);
    }
}
