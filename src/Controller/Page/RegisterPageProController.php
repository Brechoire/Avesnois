<?php

namespace App\Controller\Page;

use App\Service\PageProService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Ajout de la page PRO
 * @package App\Controller\Page
 * @Route("/profil/mapage")
 */
class RegisterPageProController extends AbstractController
{
    /**
     * @Route("/ajout", name="add_page_pro", schemes={"https"})
     * @param Request $request
     * @param PageProService $pageProService
     * @return Response
     */
    public function index(Request $request, PageProService $pageProService): Response
    {
        $id = $pageProService->getIdUser();

        $this->denyAccessUnlessGranted('ROLE_PRO');

        $check = $pageProService->PagePro($id);

        if ($check != null) {
            return $this->redirectToRoute('profile', ['id' => $id]);
        }

        $form = $pageProService->addPagePro($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Bravo, vous pouvez maintenant améliorer votre page avec les tuiles');
            return $this->redirectToRoute('home_pro_profile', ['id' => $id]);
        }

        return $this->render('pagepro/add_page_pro.html.twig', [
            'form' => $form->createView(),
            'check' => $check
        ]);
    }
}
