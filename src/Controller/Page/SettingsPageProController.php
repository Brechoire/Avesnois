<?php

namespace App\Controller\Page;

use App\Service\PageProService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Paramètre de la page PRO
 * @package App\Controller\Page
 * @Route("/profil/mapage")
 */
class SettingsPageProController extends AbstractController
{
    /**
     * @param TokenStorageInterface $tokenStorage
     * @param PageProService $pageProService
     * @param Request $request
     * @return Response
     * @Route("/{id}/parametres/{token}", name="settings_page_pro", schemes={"https"})
     */
    public function index(
        TokenStorageInterface $tokenStorage,
        PageProService $pageProService,
        Request $request
    ): Response {
        $id = $pageProService->getIdUser();
        $this->denyAccessUnlessGranted('ROLE_PRO');

        if ($id != $tokenStorage->getToken()->getUser()->getId()) {
            return $this->redirectToRoute('profile', ['id' => $id]);
        }

        $submittedToken = $request->get('token');
        if (!$this->isCsrfTokenValid('settings_page_pro', $submittedToken)) {
            return $this->redirectToRoute('home');
        }

        $check = $pageProService->PagePro($id);

        if ($check == null) {
            return $this->redirectToRoute('profile', ['id' => $id]);
        }

        $form = $pageProService->settingsPage($request, $id);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Vous avez modifié vos paramètres');
            return $this->redirectToRoute('settings_page_pro', ['id' => $id, 'token' => $submittedToken]);
        }

        return $this->render('pagepro/settings.html.twig', [
            'form' => $form->createView(),
            'check' => $check
        ]);
    }
}
