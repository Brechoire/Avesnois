<?php

namespace App\Controller\Page;

use App\Entity\ContactPro;
use App\Service\ContactService;
use App\Service\PageProService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ShowContactController
 * @package App\Controller\Page
 * @Route("/profil")
 */
class ShowContactController extends AbstractController
{
    /**
     * @Route("/message/{token}/{id}", name="show_message")
     * @param ContactPro $contactPro
     * @param PageProService $pageProService
     * @param ContactService $contactService
     * @param Request $request
     * @return Response
     */
    public function index(
        ContactPro $contactPro,
        PageProService $pageProService,
        ContactService $contactService,
        Request $request
    ): Response {

        /**
         * Check Token
         */
        $submittedToken = $request->get('token');
        if (!$this->isCsrfTokenValid('show_message', $submittedToken)) {
            return $this->redirectToRoute('home');
        }

        $this->denyAccessUnlessGranted('ROLE_PRO');

        $check = $pageProService->PagePro($contactPro->getPage());

        $contactService->updateView($contactPro->getId());

        return $this->render("profile/message.html.twig", [
            'check' => $check,
            'message' => $contactPro
        ]);
    }
}
