<?php

namespace App\Controller\Page;

use App\Service\PageProService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ShowListContact
 * @package App\Controller\Page
 * @Route("/profil/mapage")
 */
class ShowListContact extends AbstractController
{
    /**
     * @Route("/{id}/liste-messages/{token}", name="list_message", schemes={"https"}, requirements={"id"="\d+"})
     * @param PageProService $pageProService
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function index(PageProService $pageProService, $id, Request $request): Response
    {
        /**
         * Check Token
         */
        $submittedToken = $request->get('token');
        if (!$this->isCsrfTokenValid('list_message', $submittedToken)) {
            return $this->redirectToRoute('home');
        }

        $this->denyAccessUnlessGranted('ROLE_PRO');

        $list_contact = $pageProService->listContact($id);
        $check = $pageProService->PagePro($id);

        return $this->render('pagepro/list_messages.html.twig', [
            'lists' => $list_contact,
            'check' => $check
        ]);
    }
}
