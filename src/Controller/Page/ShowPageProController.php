<?php

namespace App\Controller\Page;

use App\Service\PageProService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Exception\RedirectException;

/**
 * Affichage de la page pro
 * @package App\Controller\Page
 */
class ShowPageProController extends AbstractController
{
    /**
     * @param $slug
     * @param PageProService $pageProService
     * @return Response $slug
     * @Route("/page/{slug}", name="page_pro")
     * @throws RedirectException
     */
    public function index($slug, PageProService $pageProService): Response
    {
        $page = $pageProService->showPagePro($slug);
        $pageProService->ExistPage($page);

        $pageProService->redirectPage($page->getIsPublished(), $page->getIsActivaded());

        return $this->render('pagepro/show_page.html.twig', [
            'page' => $page
        ]);
    }
}
