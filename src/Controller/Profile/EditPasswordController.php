<?php

namespace App\Controller\Profile;

use App\Service\PageProService;
use App\Service\ProfileService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class EditPasswordController
 * @package App\Controller\Profile
 * @Route("/profil")
 */
class EditPasswordController extends AbstractController
{
    /**
     * @Route("/{id}/edit-mot-de-passe/{token}", name="edit_password", schemes={"https"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param $id
     * @param TokenStorageInterface $tokenStorage
     * @param ProfileService $profileService
     * @param PageProService $pageProService
     * @return RedirectResponse|Response
     */
    public function index(
        Request $request,
        $id,
        TokenStorageInterface $tokenStorage,
        ProfileService $profileService,
        PageProService $pageProService
    ) {
        $profileService->checkGetIdUser($profileService->getIdUser(), $request->get('id'));


        $formPassword = $profileService->editPassword($request, $id);

        if ($formPassword->isSubmitted() && $formPassword->isValid()) {
            $this->addFlash('success', 'Mot de passe modifié');
            return $this->redirectToRoute('profile', ['id' => $id]);
        }

        $check = $pageProService->PagePro($id);

        /**
         * Check Token Edit
         */
        $submittedToken = $request->get('token');
        if (!$this->isCsrfTokenValid('edit_password', $submittedToken)) {
            return $this->redirectToRoute('home');
        }

        return $this->render('profile/edit_password.html.twig', [
            'formPassword' => $formPassword->createView(),
            'check' => $check
        ]);
    }
}
