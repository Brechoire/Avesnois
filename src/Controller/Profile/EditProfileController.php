<?php

namespace App\Controller\Profile;

use App\Exception\RedirectException;
use App\Service\PageProService;
use App\Service\ProfileService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class EditProfileController
 * @package App\Controller\Profile
 * @Route("/profil")
 */
class EditProfileController extends AbstractController
{

    /**
     * @Route("/{id}/edit/{token}", name="profile_edit", schemes={"https"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param $id
     * @param ProfileService $profileService
     * @param PageProService $pageProService
     * @return RedirectResponse|Response
     * @throws RedirectException
     */
    public function index(
        Request $request,
        $id,
        ProfileService $profileService,
        PageProService $pageProService
    ) {
        $profileService->checkGetIdUser($profileService->getIdUser(), $request->get('id'));


        $formEdit = $profileService->editProfileUser($request);

        if ($formEdit->isSubmitted() && $formEdit->isValid()) {
            $this->addFlash('success', 'Votre profil à bien été modifié');
            return $this->redirectToRoute('profile', ['id' => $id]);
        }

        /**
         * Check Token Edit
         */
        $submittedToken = $request->get('token');
        if (!$this->isCsrfTokenValid('profile_edit', $submittedToken)) {
            return $this->redirectToRoute('home');
        }

        $check = $pageProService->PagePro($id);

        return $this->render('profile/edit_profile.html.twig', [
            'form' => $formEdit->createView(),
            'check' => $check
        ]);
    }
}
