<?php

namespace App\Controller;

use App\Service\SearchService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SearchController
 * @package App\Controller
 */
class SearchController extends AbstractController
{
    /**
     * @Route("/recherche", name="search", schemes={"https"})
     * @param Request $request
     * @param SearchService $searchService
     * @return Response
     */
    public function search(Request $request, SearchService $searchService): Response
    {
        $form = $searchService->search($request);

        return $this->render('search.html.twig', [
            'form'     => $form['form']->createView(),
            'pages'    => $form['pages'],
            'choice'   => $form['choice'],
            'services' => $form['services'],
            'city'     => $form['city'],
            'search'   => $form['search']
        ]);
    }
}
