<?php

namespace App\DataFixtures;

use App\Entity\City;
use App\Entity\NafCode;
use App\Entity\Page;
use App\Entity\Service;
use App\Entity\Social;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class AppFixtures extends Fixture
{

    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * AppFixtures constructor.
     * @param UserPasswordEncoderInterface $encoder
     * @param TokenGeneratorInterface $tokenGenerator
     */
    public function __construct(UserPasswordEncoderInterface $encoder, TokenGeneratorInterface $tokenGenerator)
    {
        $this->encoder = $encoder;
        $this->tokenGenerator = $tokenGenerator;
    }

    public function load(ObjectManager $manager)
    {

        $faker = Faker\Factory::create('fr_FR');
        $faker_color = Faker\Provider\Color::hexColor();
        $faker_img = Faker\Provider\Image::imageUrl($width = 640, $height = 480);

        for ($i = 0; $i < 10; $i++) {
            for ($c = 0; $c < 15; $c++) {
                $city = new City();
                $city->setCity($faker->city);
                $city->setPostal($faker->postcode);

                $naf = new NafCode();
                $naf->setNafCode($faker->title . ' ' . $c++);
                $naf->setTitledNaf65($faker->title . ' ' . $c++);
                $naf->setTitledNaf($faker->title . ' ' . $c++);
                $naf->setTitledNaf40($faker->title . ' ' . $c++);
                $manager->persist($city);
                $manager->persist($naf);
            }

            $token = substr($faker->name, 0, 3)
                . rand(15, 629) . '-'
                . rand(300, 9000) . '-'
                . substr(
                    $this->tokenGenerator->generateToken(),
                    0,
                    5
                );

            $user = new User();

            $user->setUsername($faker->name);
            $user->setEmail($faker->email);
            $user->setFirstName($faker->firstName);
            $user->setLastName($faker->lastName);
            $user->setRoles(["ROLE_PRO"]);
            $user->setCreatedIp("127.0.0.1");
            $user->setTypeUser(1);
            $user->setHostName('PC de ' . $faker->name);
            $user->setToken($token);
            $user->setCity($city);
            $user->setActive(1);

            $password = $this->encoder->encodePassword($user, 'jerome');
            $user->setPassword($password);

            $manager->persist($user);

            $page = new Page();

            $page->setCity($city)
                  ->setNafCode($naf)
                  ->setName($faker->jobTitle)
                  ->setUser($user)
                  ->setDescription($faker->text)
                  ->setSiret('01234567891234')
                  ->setAddress($faker->address)
                  ->setTel1('0102030405')
                  ->setAddressPublished(0)
                  ->setSiretPublished(1)
                  ->setTel1Published(1)
                  ->setEmail1Published(1)
                  ->setIsActivaded(1)
                  ->setIsPublished(1)
                  ->setEmail1($faker->email);

            $manager->persist($page);

            $social = new Social();
            $social->setPage($page);
            $manager->persist($social);

            for ($s = 0; $s < 5; $s++) {
                $service = new Service();
                $service->setTitle($faker->jobTitle)
                        ->setImgService($faker_img)
                        ->setContentBack($faker->text(150))
                        ->setContent($faker->text(150))
                        ->setDescription($faker->text(150))
                        ->setColorBack($faker_color)
                        ->setTag('tag')
                        ->setPage($page)
                ;

                $manager->persist($service);
            }
        }

        $manager->flush();
    }
}
