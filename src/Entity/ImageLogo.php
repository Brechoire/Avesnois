<?php

namespace App\Entity;

use App\Repository\ImageLogoRepository;
use Doctrine\ORM\Mapping as ORM;
use Serializable;

/**
 * @ORM\Entity(repositoryClass=ImageLogoRepository::class)
 */
class ImageLogo implements Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity=Page::class, inversedBy="imageLogo", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $page;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIdPage(): ?Page
    {
        return $this->page;
    }

    public function setIdPage(Page $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function serialize(): string
    {
        return serialize([
            $this->id,
            $this->page
        ]);
    }

    public function unserialize($serialized): void
    {
        list (
            $this->id,
            $this->page
            ) = unserialize($serialized);
    }
}
