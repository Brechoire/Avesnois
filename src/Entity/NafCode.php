<?php

namespace App\Entity;

use App\Repository\NafCodeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NafCodeRepository::class)
 */
class NafCode
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $naf_code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titled_naf;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titled_naf_65;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titled_naf_40;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNafCode(): ?string
    {
        return $this->naf_code;
    }

    public function setNafCode(string $naf_code): self
    {
        $this->naf_code = $naf_code;

        return $this;
    }

    public function getTitledNaf(): ?string
    {
        return $this->titled_naf;
    }

    public function setTitledNaf(string $titled_naf): self
    {
        $this->titled_naf = $titled_naf;

        return $this;
    }

    public function getTitledNaf65(): ?string
    {
        return $this->titled_naf_65;
    }

    public function setTitledNaf65(string $titled_naf_65): self
    {
        $this->titled_naf_65 = $titled_naf_65;

        return $this;
    }

    public function getTitledNaf40(): ?string
    {
        return $this->titled_naf_40;
    }

    public function setTitledNaf40(string $titled_naf_40): self
    {
        $this->titled_naf_40 = $titled_naf_40;

        return $this;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->naf_code;
    }

    /**
     * @return string
     */
    public function getNafTitled(): string
    {
        return $this->naf_code . ' - ' . $this->titled_naf_40;
    }
}
