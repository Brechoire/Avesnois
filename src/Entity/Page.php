<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Serializable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PageRepository")
 * @UniqueEntity(fields={"siret"}, message="Le numéro de SIRET est déjà utilisé.")
 * @UniqueEntity(fields={"name"}, message="Le nom de l'entreprise est déjà utilisé.")
 * @UniqueEntity(fields={"email1"}, message="L'adresse email est déjà utilisé.")
 * @UniqueEntity(fields={"email2"}, message="L'adresse email est déjà utilisé.")
 * @UniqueEntity(fields={"tel1"}, message="Le numéro de téléphone est déjà utilisé.")
 * @UniqueEntity(fields={"tel2"}, message="Le numéro de téléphone est déjà utilisé.")
 * @ORM\Table(name="page", indexes={@ORM\Index(columns={"name"}, flags={"fulltext"})})
 */
class Page implements Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=90)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=14, nullable=false)
     */
    private $siret;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\NafCode")
     * @ORM\JoinColumn(nullable=false)
     */
    private $naf_code;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     */
    private $tel1;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $tel2;

    /**
     * @ORM\Column(type="string", length=80, nullable=false)
     */
    private $email1;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     */
    private $email2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="boolean")
     */
    private $valide_admin;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="id_user", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_activaded;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_published;

    /**
     * @ORM\Column(type="boolean")
     */
    private $address_published;

    /**
     * @ORM\Column(type="boolean")
     */
    private $tel1_published;

    /**
     * @ORM\Column(type="boolean")
     */
    private $siret_published;

    /**
     * @ORM\Column(type="boolean")
     */
    private $email1_published;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $fax;

    /**
     * @ORM\OneToOne(targetEntity=ImageLogo::class, mappedBy="page", cascade={"persist", "remove"})
     */
    private $imageLogo;

    /**
     * @ORM\OneToOne(targetEntity=ImageBan::class, mappedBy="page", cascade={"persist", "remove"})
     */
    private $imageBan;

    /**
     * @ORM\OneToOne(targetEntity=Schedule::class, mappedBy="page", cascade={"persist", "remove"})
     */
    private $schedule;

    /**
     * @ORM\Column(type="boolean")
     */
    private $schedule_published;

    /**
     * @ORM\OneToOne(targetEntity=Social::class, mappedBy="page", cascade={"persist", "remove"})
     */
    private $social;

    /**
     * @ORM\OneToMany(targetEntity=Service::class, mappedBy="page")
     */
    private $services;

    /**
     * @ORM\OneToMany(targetEntity=ContactPro::class, mappedBy="page")
     */
    private $contacts;
    private $id_user;

    /**
     * Page constructor.
     */
    public function __construct()
    {
        $this->created_at = new DateTime('now');
        $this->valide_admin = 0;
        $this->is_published = 0;
        $this->is_activaded = 0;
        $this->schedule_published = 0;
        $this->services = new ArrayCollection();
        $this->contacts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getIdUser(): ?Page
    {
        return $this->id_user;
    }

    public function setIdUser(City $id_user): self
    {
        $this->id_user = $id_user;

        // set the owning side of the relation if necessary
        if ($id_user->getUser() !== $this) {
            $id_user->setUser($this);
        }

        return $this;
    }

    public function getTel1(): ?string
    {
        return $this->tel1;
    }

    public function setTel1(?string $tel1): self
    {
        $this->tel1 = $tel1;

        return $this;
    }

    public function getTel2(): ?string
    {
        return $this->tel2;
    }

    public function setTel2(?string $tel2): self
    {
        $this->tel2 = $tel2;

        return $this;
    }

    public function getEmail1(): ?string
    {
        return $this->email1;
    }

    public function setEmail1(?string $email1): self
    {
        $this->email1 = $email1;

        return $this;
    }

    public function getEmail2(): ?string
    {
        return $this->email2;
    }

    public function setEmail2(?string $email2): self
    {
        $this->email2 = $email2;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getValideAdmin(): ?bool
    {
        return $this->valide_admin;
    }

    public function setValideAdmin(bool $valide_admin): self
    {
        $this->valide_admin = $valide_admin;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getNafCode(): ?NafCode
    {
        return $this->naf_code;
    }

    public function setNafCode(?NafCode $naf_code): self
    {
        $this->naf_code = $naf_code;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function getIsActivaded(): ?bool
    {
        return $this->is_activaded;
    }

    public function setIsActivaded(bool $is_activaded): self
    {
        $this->is_activaded = $is_activaded;

        return $this;
    }

    public function getIsPublished(): ?bool
    {
        return $this->is_published;
    }

    public function setIsPublished(bool $is_published): self
    {
        $this->is_published = $is_published;

        return $this;
    }

    public function getAddressPublished(): ?bool
    {
        return $this->address_published;
    }

    public function setAddressPublished(bool $address_published): self
    {
        $this->address_published = $address_published;

        return $this;
    }

    public function getTel1Published(): ?bool
    {
        return $this->tel1_published;
    }

    public function setTel1Published(bool $tel1_published): self
    {
        $this->tel1_published = $tel1_published;

        return $this;
    }

    public function getSiretPublished(): ?bool
    {
        return $this->siret_published;
    }

    public function setSiretPublished(bool $siret_published): self
    {
        $this->siret_published = $siret_published;

        return $this;
    }


    public function getSchedulePublished(): ?bool
    {
        return $this->schedule_published;
    }

    public function setSchedulePublished(bool $schedule_published): self
    {
        $this->schedule_published = $schedule_published;

        return $this;
    }


    public function getEmail1Published(): ?bool
    {
        return $this->email1_published;
    }

    public function setEmail1Published(bool $email1_published): self
    {
        $this->email1_published = $email1_published;

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(?string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getImageLogo(): ?ImageLogo
    {
        return $this->imageLogo;
    }

    public function setImageLogo(ImageLogo $imageLogo): self
    {
        $this->imageLogo = $imageLogo;

        // set the owning side of the relation if necessary
        if ($imageLogo->getIdPage() !== $this) {
            $imageLogo->setIdPage($this);
        }

        return $this;
    }

    public function getImageBan(): ?ImageBan
    {
        return $this->imageBan;
    }

    public function setImageBan(ImageBan $imageBan): self
    {
        $this->imageBan = $imageBan;

        // set the owning side of the relation if necessary
        if ($imageBan->getIdPage() !== $this) {
            $imageBan->setIdPage($this);
        }

        return $this;
    }

    public function getSocial(): ?Social
    {
        return $this->social;
    }

    public function setSocial(?Social $social): self
    {
        $this->social = $social;

        // set (or unset) the owning side of the relation if necessary
        $newPage = null === $social ? null : $this;
        if ($social->getPage() !== $newPage) {
            $social->setPage($newPage);
        }

        return $this;
    }

    public function getSchedule(): ?Schedule
    {
        return $this->schedule;
    }

    public function setSchedule(?Schedule $schedule): self
    {
        $this->schedule = $schedule;

        // set (or unset) the owning side of the relation if necessary
        $newPage = null === $schedule ? null : $this;
        if ($schedule->getPage() !== $newPage) {
            $schedule->setPage($newPage);
        }

        return $this;
    }

    public function serialize(): string
    {
        return serialize([
            $this->id,
            $this->imageLogo,
            $this->social,
            $this->schedule,
            $this->naf_code
        ]);
    }

    public function unserialize($serialized): void
    {
        list (
            $this->id,
            $this->imageLogo,
            $this->social,
            $this->schedule,
            $this->naf_code
            ) = unserialize($serialized);
    }

    /**
     * @return Collection|Service[]
     */
    public function getServices(): Collection
    {
        return $this->services;
    }

    public function addService(Service $service): self
    {
        if (!$this->services->contains($service)) {
            $this->services[] = $service;
            $service->setPage($this);
        }

        return $this;
    }

    public function removeService(Service $service): self
    {
        if ($this->services->contains($service)) {
            $this->services->removeElement($service);
            // set the owning side to null (unless already changed)
            if ($service->getPage() === $this) {
                $service->setPage(null);
            }
        }

        return $this;
    }

    // Contact

    /**
     * @return Collection|ContactPro []
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    /**
     * @param ContactPro $contact
     * @return $this
     */
    public function addContact(ContactPro $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setPage($this);
        }

        return $this;
    }

    /**
     * @param ContactPro $contact
     * @return $this
     */
    public function removeContact(ContactPro $contact): self
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
            // set the owning side to null (unless already changed)
            if ($contact->getPage() === $this) {
                $contact->setPage(null);
            }
        }

        return $this;
    }
}
