<?php

namespace App\Entity;

use App\Repository\ScheduleRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Serializable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScheduleRepository", repositoryClass=ScheduleRepository::class)
 */
class Schedule implements Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $mon_pm;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $mon_am;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $tue_pm;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $tue_am;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $wed_pm;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $wed_am;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $thu_pm;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $thu_am;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $fri_pm;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $fri_am;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $sat_pm;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $sat_am;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $sun_pm;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $sun_am;

    /** End */

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $mon_pm_end;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $mon_am_end;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $tue_pm_end;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $tue_am_end;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $wed_pm_end;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $wed_am_end;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $thu_pm_end;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $thu_am_end;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $fri_pm_end;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $fri_am_end;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $sat_pm_end;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $sat_am_end;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $sun_pm_end;

    /**
     * @ORM\Column(type="time", length=255, nullable=true)
     */
    private $sun_am_end;

    /**
     * Fermeture
     */

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $close_mon;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $close_tue;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $close_wed;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $close_thu;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $close_fri;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $close_sat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $close_sun;

    /**
     * @ORM\OneToOne(targetEntity=Page::class, inversedBy="schedule", cascade={"persist", "remove"})
     */
    private $page;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMonPm(): ?DateTime
    {
        if ($this->mon_pm == null) {
            $this->mon_pm = new DateTime('08:00');
        }

        return $this->mon_pm;
    }

    public function setMonPm(?DateTime $mon_pm): self
    {
        $this->mon_pm = $mon_pm;

        return $this;
    }

    public function getMonAm(): ?DateTime
    {
        if ($this->mon_am == null) {
            $this->mon_am = new DateTime('13:00');
        }

        return $this->mon_am;
    }

    public function setMonAm(?DateTime $mon_am): self
    {
        $this->mon_am = $mon_am;

        return $this;
    }

    public function getTuePm(): ?DateTime
    {

        if ($this->tue_pm == null) {
            $this->tue_pm = new DateTime('08:00');
        }

        return $this->tue_pm;
    }

    public function setTuePm(?DateTime $tue_pm): self
    {
        $this->tue_pm = $tue_pm;

        return $this;
    }

    public function getTueAm(): ?DateTime
    {
        if ($this->tue_am == null) {
            $this->tue_am = new DateTime('13:00');
        }

        return $this->tue_am;
    }

    public function setTueAm(?DateTime $tue_am): self
    {
        $this->tue_am = $tue_am;

        return $this;
    }

    public function getWedPm(): ?DateTime
    {
        if ($this->wed_pm == null) {
            $this->wed_pm = new DateTime('08:00');
        }

        return $this->wed_pm;
    }

    public function setWedPm(?DateTime $wed_pm): self
    {
        $this->wed_pm = $wed_pm;

        return $this;
    }

    public function getWedAm(): ?DateTime
    {
        if ($this->wed_am == null) {
            $this->wed_am = new DateTime('13:00');
        }

        return $this->wed_am;
    }

    public function setWedAm(?DateTime $wed_am): self
    {
        $this->wed_am = $wed_am;

        return $this;
    }

    public function getThuPm(): ?DateTime
    {
        if ($this->thu_pm == null) {
            $this->thu_pm = new DateTime('08:00');
        }

        return $this->thu_pm;
    }

    public function setThuPm(?DateTime $thu_pm): self
    {
        $this->thu_pm = $thu_pm;

        return $this;
    }

    public function getThuAm(): ?DateTime
    {
        if ($this->thu_am == null) {
            $this->thu_am = new DateTime('13:00');
        }

        return $this->thu_am;
    }

    public function setThuAm(?DateTime $thu_am): self
    {
        $this->thu_am = $thu_am;

        return $this;
    }

    public function getFriPm(): ?DateTime
    {
        if ($this->fri_pm == null) {
            $this->fri_pm = new DateTime('08:00');
        }

        return $this->fri_pm;
    }

    public function setFriPm(?DateTime $fri_pm): self
    {
        $this->fri_pm = $fri_pm;

        return $this;
    }

    public function getFriAm(): ?DateTime
    {
        if ($this->fri_am == null) {
            $this->fri_am = new DateTime('13:00');
        }

        return $this->fri_am;
    }

    public function setFriAm(?DateTime $fri_am): self
    {
        $this->fri_am = $fri_am;

        return $this;
    }

    public function getSatPm(): ?DateTime
    {
        if ($this->sat_pm == null) {
            $this->sat_pm = new DateTime('08:00');
        }

        return $this->sat_pm;
    }

    public function setSatPm(?DateTime $sat_pm): self
    {
        $this->sat_pm = $sat_pm;

        return $this;
    }

    public function getSatAm(): ?DateTime
    {
        if ($this->sat_am == null) {
            $this->sat_am = new DateTime('13:00');
        }

        return $this->sat_am;
    }

    public function setSatAm(?DateTime $sat_am): self
    {
        $this->sat_am = $sat_am;

        return $this;
    }

    public function getSunPm(): ?DateTime
    {
        if ($this->sun_pm == null) {
            $this->sun_pm = new DateTime('08:00');
        }

        return $this->sun_pm;
    }

    public function setSunPm(?DateTime $sun_pm): self
    {
        $this->sun_pm = $sun_pm;

        return $this;
    }

    public function getSunAm(): ?DateTime
    {
        if ($this->sun_am == null) {
            $this->sun_am = new DateTime('13:00');
        }

        return $this->sun_am;
    }

    public function setSunAm(?DateTime $sun_am): self
    {
        $this->sun_am = $sun_am;

        return $this;
    }

    public function getCloseMon(): ?string
    {
        return $this->close_mon;
    }

    public function setCloseMon(?string $close_mon): self
    {
        $this->close_mon = $close_mon;

        return $this;
    }

    public function getCloseTue(): ?string
    {
        return $this->close_tue;
    }

    public function setCloseTue(?string $close_tue): self
    {
        $this->close_tue = $close_tue;

        return $this;
    }

    public function getCloseWed(): ?string
    {
        return $this->close_wed;
    }

    public function setCloseWed(?string $close_wed): self
    {
        $this->close_wed = $close_wed;

        return $this;
    }

    public function getCloseThu(): ?string
    {
        return $this->close_thu;
    }

    public function setCloseThu(?string $close_thu): self
    {
        $this->close_thu = $close_thu;

        return $this;
    }

    public function getCloseFri(): ?string
    {
        return $this->close_fri;
    }

    public function setCloseFri(?string $close_fri): self
    {
        $this->close_fri = $close_fri;

        return $this;
    }

    public function getCloseSat(): ?string
    {
        return $this->close_sat;
    }

    public function setCloseSat(?string $close_sat): self
    {
        $this->close_sat = $close_sat;

        return $this;
    }

    public function getCloseSun(): ?string
    {
        return $this->close_sun;
    }

    public function setCloseSun(?string $close_sun): self
    {
        $this->close_sun = $close_sun;

        return $this;
    }

    public function getPage(): ?page
    {
        return $this->page;
    }

    public function setPage(?page $page): self
    {
        $this->page = $page;

        return $this;
    }


    public function getMonPmEnd(): ?DateTime
    {
        if ($this->mon_pm_end == null) {
            $this->mon_pm_end = new DateTime('12:00');
        }

        return $this->mon_pm_end;
    }

    public function setMonPmEnd(?DateTime $mon_pm_end): self
    {
        $this->mon_pm_end = $mon_pm_end;

        return $this;
    }

    public function getMonAmEnd(): ?DateTime
    {
        if ($this->mon_am_end == null) {
            $this->mon_am_end = new DateTime('18:00');
        }

        return $this->mon_am_end;
    }

    public function setMonAmEnd(?DateTime $mon_am_end): self
    {
        $this->mon_am_end = $mon_am_end;

        return $this;
    }

    public function getTuePmEnd(): ?DateTime
    {
        if ($this->tue_pm_end == null) {
            $this->tue_pm_end = new DateTime('12:00');
        }

        return $this->tue_pm_end;
    }

    public function setTuePmEnd(?DateTime $tue_pm_end): self
    {
        $this->tue_pm_end = $tue_pm_end;

        return $this;
    }

    public function getTueAmEnd(): ?DateTime
    {
        if ($this->tue_am_end == null) {
            $this->tue_am_end = new DateTime('18:00');
        }

        return $this->tue_am_end;
    }

    public function setTueAmEnd(?DateTime $tue_am_end): self
    {
        $this->tue_am_end = $tue_am_end;

        return $this;
    }

    public function getWedPmEnd(): ?DateTime
    {
        if ($this->wed_pm_end == null) {
            $this->wed_pm_end = new DateTime('12:00');
        }

        return $this->wed_pm_end;
    }

    public function setWedPmEnd(?DateTime $wed_pm_end): self
    {
        $this->wed_pm_end = $wed_pm_end;

        return $this;
    }

    public function getWedAmEnd(): ?DateTime
    {
        if ($this->wed_am_end == null) {
            $this->wed_am_end = new DateTime('18:00');
        }

        return $this->wed_am_end;
    }

    public function setWedAmEnd(?DateTime $wed_am_end): self
    {
        $this->wed_am_end = $wed_am_end;

        return $this;
    }

    public function getThuPmEnd(): ?DateTime
    {
        if ($this->thu_pm_end == null) {
            $this->thu_pm_end = new DateTime('12:00');
        }

        return $this->thu_pm_end;
    }

    public function setThuPmEnd(?DateTime $thu_pm_end): self
    {
        $this->thu_pm_end = $thu_pm_end;

        return $this;
    }

    public function getThuAmEnd(): ?DateTime
    {
        if ($this->thu_am_end == null) {
            $this->thu_am_end = new DateTime('18:00');
        }

        return $this->thu_am_end;
    }

    public function setThuAmEnd(?DateTime $thu_am_end): self
    {
        $this->thu_am_end = $thu_am_end;

        return $this;
    }

    public function getFriPmEnd(): ?DateTime
    {
        if ($this->fri_pm_end == null) {
            $this->fri_pm_end = new DateTime('12:00');
        }

        return $this->fri_pm_end;
    }

    public function setFriPmEnd(?DateTime $fri_pm_end): self
    {
        $this->fri_pm_end = $fri_pm_end;

        return $this;
    }

    public function getFriAmEnd(): ?DateTime
    {
        if ($this->fri_am_end == null) {
            $this->fri_am_end = new DateTime('18:00');
        }

        return $this->fri_am_end;
    }

    public function setFriAmEnd(?DateTime $fri_am_end): self
    {
        $this->fri_am_end = $fri_am_end;

        return $this;
    }

    public function getSatPmEnd(): ?DateTime
    {
        if ($this->sat_pm_end == null) {
            $this->sat_pm_end = new DateTime('12:00');
        }

        return $this->sat_pm_end;
    }

    public function setSatPmEnd(?DateTime $sat_pm_end): self
    {
        $this->sat_pm_end = $sat_pm_end;

        return $this;
    }

    public function getSatAmEnd(): ?DateTime
    {
        if ($this->sat_am_end == null) {
            $this->sat_am_end = new DateTime('18:00');
        }

        return $this->sat_am_end;
    }

    public function setSatAmEnd(?DateTime $sat_am_end): self
    {
        $this->sat_am_end = $sat_am_end;

        return $this;
    }

    public function getSunPmEnd(): ?DateTime
    {
        if ($this->sun_pm_end == null) {
            $this->sun_pm_end = new DateTime('12:00');
        }

        return $this->sun_pm_end;
    }

    public function setSunPmEnd(?DateTime $sun_pm_end): self
    {
        $this->sun_pm_end = $sun_pm_end;

        return $this;
    }

    public function getSunAmEnd(): ?DateTime
    {
        if ($this->sun_am_end == null) {
            $this->sun_am_end = new DateTime('18:00');
        }

        return $this->sun_am_end;
    }

    public function setSunAmEnd(?DateTime $sun_am_end): self
    {
        $this->sun_am_end = $sun_am_end;

        return $this;
    }

    public function serialize(): string
    {
        return serialize([
            $this->id,
            $this->page,
        ]);
    }


    public function unserialize($serialized): void
    {
        list (
            $this->id,
            $this->page,
            ) = unserialize($serialized);
    }
}
