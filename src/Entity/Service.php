<?php

namespace App\Entity;

use App\Repository\ServiceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ServiceRepository::class)
 */
class Service
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity=Page::class, inversedBy="services")
     */
    private $page;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imgService;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $content_back;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $color_back;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tag;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getPage(): ?Page
    {
        return $this->page;
    }

    public function setPage(?Page $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getImgService(): ?string
    {
        return $this->imgService;
    }

    public function setImgService(string $imgService): self
    {
        $this->imgService = $imgService;

        return $this;
    }

    public function getContentBack(): ?string
    {
        return $this->content_back;
    }

    public function setContentBack(?string $content_back): self
    {
        $this->content_back = $content_back;

        return $this;
    }


    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getColorBack(): ?string
    {
        return $this->color_back;
    }

    public function setColorBack(?string $color_back): self
    {
        $this->color_back = $color_back;

        return $this;
    }

    public function getTag(): ?string
    {
        return $this->tag;
    }

    public function setTag(string $tag): self
    {
        $this->tag = $tag;

        return $this;
    }
}
