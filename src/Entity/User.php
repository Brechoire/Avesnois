<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class User
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="L'email est déjà utilisé.")
 * @UniqueEntity(fields={"username"}, message="Le nom d'utilisateur est déjà utilisé.")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", nullable=false)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=60, nullable=false)
     */
    private $username;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="boolean")
     */
    private $type_user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $created_ip;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $last_connection;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $last_ip;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ConnectionHistory", mappedBy="user")
     */
    private $connectionHistories;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $host_name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\City")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Page", mappedBy="user", cascade={"persist", "remove"})
     */
    private $id_user;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * @ORM\OneToMany(targetEntity=ContactPro::class, mappedBy="user")
     */
    private $contactPros;


    public function __construct()
    {
        $this->created_at = new DateTime('now');
        $this->active = false;
        $this->type_user = false;
        $this->roles = ['ROLE_USER'];
        $this->last_ip = $_SERVER['REMOTE_ADDR'] ?? '127.0.0.1';
        $this->connectionHistories = new ArrayCollection();
        $this->contactPros = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
//        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

//    /**
//     * @see UserInterface
//     */
//    public function getPassword(): string
//    {
//        return (string) $this->password;
//    }
//
//    public function setPassword(string $password): self
//    {
//        $this->password = $password;
//
//        return $this;
//    }
    /**
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getTypeUser(): ?bool
    {
        return $this->type_user;
    }

    public function setTypeUser(bool $type_user): self
    {
        $this->type_user = $type_user;

        return $this;
    }

    public function getCreatedIp(): ?string
    {
        return $this->created_ip;
    }

    public function setCreatedIp(string $created_ip): self
    {
        $this->created_ip = $created_ip;

        return $this;
    }

    public function getLastConnection(): ?DateTimeInterface
    {
        return $this->last_connection;
    }

    public function setLastConnection(DateTimeInterface $last_connection): self
    {
        $this->last_connection = $last_connection;

        return $this;
    }

    public function getLastIp(): ?string
    {
        return $this->last_ip;
    }

    public function setLastIp(string $last_ip): self
    {
        $this->last_ip = $last_ip;

        return $this;
    }

    /**
     * @return Collection|ConnectionHistory[]
     */
    public function getConnectionHistories(): Collection
    {
        return $this->connectionHistories;
    }

    public function addConnectionHistory(ConnectionHistory $connectionHistory): self
    {
        if (!$this->connectionHistories->contains($connectionHistory)) {
            $this->connectionHistories[] = $connectionHistory;
            $connectionHistory->setUser($this);
        }

        return $this;
    }

    public function removeConnectionHistory(ConnectionHistory $connectionHistory): self
    {
        if ($this->connectionHistories->contains($connectionHistory)) {
            $this->connectionHistories->removeElement($connectionHistory);
            // set the owning side to null (unless already changed)
            if ($connectionHistory->getUser() === $this) {
                $connectionHistory->setUser(null);
            }
        }

        return $this;
    }

    public function getHostName(): ?string
    {
        return $this->host_name;
    }

    public function setHostName(string $host_name): self
    {
        $this->host_name = $host_name;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getIdUser(): ?Page
    {
        return $this->id_user;
    }

    public function setIdUser(Page $id_user): self
    {
        $this->id_user = $id_user;

        // set the owning side of the relation if necessary
        if ($id_user->getUser() !== $this) {
            $id_user->setUser($this);
        }

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return Collection|ContactPro[]
     */
    public function getContactPros(): Collection
    {
        return $this->contactPros;
    }

    public function addContactPro(ContactPro $contactPro): self
    {
        if (!$this->contactPros->contains($contactPro)) {
            $this->contactPros[] = $contactPro;
            $contactPro->setUser($this);
        }

        return $this;
    }

    public function removeContactPro(ContactPro $contactPro): self
    {
        if ($this->contactPros->contains($contactPro)) {
            $this->contactPros->removeElement($contactPro);
            // set the owning side to null (unless already changed)
            if ($contactPro->getUser() === $this) {
                $contactPro->setUser(null);
            }
        }

        return $this;
    }
}
