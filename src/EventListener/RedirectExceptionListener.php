<?php

namespace App\EventListener;

use App\Exception\RedirectException;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

/**
 * Listen the kernel.exception event in order to redirect to an url
 *
 * @author Grégory LEFER <contact@glefer.fr>
 */
class RedirectExceptionListener
{

    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event)
    {
        if (($exception = $event->getThrowable()) instanceof RedirectException) {
            $event->setResponse($exception->getResponse());
        }
    }
}
