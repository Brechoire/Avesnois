<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class RegistrationFormType
 * @package App\Form
 */
class EditProfileUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', EmailType::class, [
                'label' => 'Email *',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Attention le champs est vide'
                    ]),
                    new Email([
                        'message' => '{{value}} n\'est pas une adrese email valide'
                    ])
                ]
            ])
            ->add('username', TextType::class, [
                'label' => 'Nom d\'utilisateur *',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Attention le champs est vide'
                    ]),
                    new Length([
                        'min' => 4,
                        'minMessage' => 'Votre nom d\'utilisateur doit avoir 4 caractères minimum',
                        'max' => 20,
                        'maxMessage' => 'Votre nom d\'utilisateur doit avoir 20 caractères maximum',
                    ]),
                ]
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Prénom *',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Attention le champs est vide'
                    ]),
                ]
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Nom *',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Attention le champs est vide'
                    ]),
                ]
            ])
            ->add('city', EntityType::class, [
                'class' => City::class,
                'label' => 'Ville',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.city', 'ASC');
                },
                'choice_label' => 'getCityPostal',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
