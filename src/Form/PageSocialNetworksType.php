<?php

namespace App\Form;

use App\Entity\Social;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PageSocialNetworksType
 * @package App\Form
 */
class PageSocialNetworksType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('facebook', TextType::class, [
                'label' => 'Facebook',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Ex: monavesnois'
                ],
                'required' => false,
            ])
            ->add('twitter', TextType::class, [
                'label' => 'Twitter',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Ex: monavesnois'
                ],
                'required' => false
            ])
            ->add('instagram', TextType::class, [
                'label' => 'Instagram',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Ex: monavesnois'
                ],
                'required' => false
            ])
            ->add('linkedin', TextType::class, [
                'label' => 'LinkedIn',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Ex: monavesnois'
                ],
                'required' => false
            ])
            ->add('youtube', TextType::class, [
                'label' => 'Youtube',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Ex: monavesnois'
                ],
                'required' => false
            ])
            ->add('github', TextType::class, [
                'label' => 'Github',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Ex: monavesnois'
                ],
                'required' => false
            ])
            ->add('twitch', TextType::class, [
                'label' => 'Twitch',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Ex: monavesnois'
                ],
                'required' => false
            ])
            ->add('tumblr', TextType::class, [
                'label' => 'Tumblr',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Ex: monavesnois'
                ],
                'required' => false
            ])
            ->add('viadeo', TextType::class, [
                'label' => 'Viadeo',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Ex: monavesnois'
                ],
                'required' => false
            ])
            ->add('pinterest', TextType::class, [
                'label' => 'Pinterest',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Ex: monavesnois'
                ],
                'required' => false
            ])
            ->add('flickr', TextType::class, [
                'label' => 'Flickr',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Ex: monavesnois'
                ],
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Social::class,
        ]);
    }
}
