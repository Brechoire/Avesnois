<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\NafCode;
use App\Entity\Page;
use Doctrine\ORM\EntityRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Formulaire de création de la page PRO
 * @package App\Form
 */
class PageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom de l\'entreprise *',
                'required' => true,
                'constraints' => [
                    new Length([
                        'min' => 3,
                        'minMessage' => 'Le nom doit faire minimum 3 caractères',
                        'max' => 90,
                        'maxMessage' => 'Le nom doit avoir 90 caractères maximum'
                    ]),
                    new NotBlank([
                        'message' => 'Le nom est requis'
                    ])
                ]
            ])
            ->add('description', CKEditorType::class, [
                'label' => 'Description de votre entreprise 250 caractères minimum *',
                'config' => [
                    'toolbar' => 'my_toolbar_2',
                    'required' => true
                ],
                'constraints' => [
                    new Length([
                        'min' => 250,
                        'minMessage' => 'La description doit faire minimum 250 caractères',
                    ]),
                    new NotBlank([
                        'message' => 'Une description est requis'
                    ])
                ]
            ])
            ->add('siret', TextType::class, [
                'label' => 'Numéro de SIRET *',
                'attr' => [
                    'class' => 'form-control'
                ],
                'constraints' => [
                    new Length([
                        'min' => 14,
                        'minMessage' => 'Le numéro de SIRET doit faire 14 caractères'
                    ])
                ]
            ])
            ->add('fax', TextType::class, [
                'label' => 'Fax',
                'required' => false,
                'constraints' => [
                    new Length([
                        'min' => 10,
                        'minMessage' => 'Le numéro doit avoir 10 chiffres',
                        'max' => 10,
                        'maxMessage' => 'Le numéro doit avoir 10 chiffres'
                    ])
                ]
            ])
            ->add('address', TextType::class, [
                'label' => 'Adresse *',
                'attr' => [
                    'class' => 'form-control'
                ],
            ])
            ->add('city', EntityType::class, [
                'class' => City::class,
                'label' => 'Ville',
                'attr' => [
                    'class' => 'form-control'
                ],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.city', 'ASC');
                },
                'choice_label' => 'getCityPostal',
            ])
            ->add('nafCode', EntityType::class, [
                'class' => NafCode::class,
                'label' => 'Code NAF / APE',
                'attr' => [
                    'class' => 'form-control'
                ],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->orderBy('n.titled_naf_65', 'ASC');
                },
                'choice_label' => 'getNafTitled',
            ])
            ->add('tel1', TextType::class, [
                'label' => 'Téléphone *',
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'min' => 10,
                        'minMessage' => 'Le numéro doit avoir 10 chiffres',
                        'max' => 10,
                        'maxMessage' => 'Le numéro doit avoir 10 chiffres'
                    ])
                ]
            ])
            ->add('tel2', TextType::class, [
                'label' => 'Téléphone 2',
                'required' => false,
                'constraints' => [
                    new Length([
                        'min' => 10,
                        'minMessage' => 'Le numéro doit avoir 10 chiffres',
                        'max' => 10,
                        'maxMessage' => 'Le numéro doit avoir 10 chiffres'
                    ])
                ]
            ])
            ->add('email1', TextType::class, [
                'label' => 'Email *',
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => true,
                'constraints' => [
                    new Email([
                        'message' => '{{ value }} n\'est pas une adrese email valide'
                    ]),
                    new NotBlank([
                        'message' => 'L\'adresse email ne peut pas être vide'
                    ])
                ]
            ])
            ->add('email2', TextType::class, [
                'label' => 'Email 2',
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => false,
                'constraints' => [
                    new Email([
                        'message' => '{{value}} n\'est pas une adrese email valide'
                    ])
                ]
            ])
            ->add('website', TextType::class, [
                'label' => 'Site Web',
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Page::class,
        ]);
    }
}
