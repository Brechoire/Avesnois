<?php

namespace App\Form;

use App\Entity\Schedule;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ScheduleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mon_pm', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time mon_pm'
                ],
            ])
            ->add('mon_pm_end', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',

                'attr' => [
                    'class' => 'time mon_pm'
                ],

            ])
            ->add('mon_am', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time mon_am'
                ],
            ])
            ->add('mon_am_end', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',

                'attr' => [
                    'class' => 'time mon_am'
                ],
            ])
            ->add('tue_pm', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time tue_pm'
                ],
            ])
            ->add('tue_pm_end', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',

                'attr' => [
                    'class' => 'time tue_pm'
                ],
            ])
            ->add('tue_am', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time tue_am'
                ],
            ])
            ->add('tue_am_end', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',

                'attr' => [
                    'class' => 'time tue_am'
                ],
            ])
            ->add('wed_pm', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time wed_pm'
                ],
            ])
            ->add('wed_pm_end', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',

                'attr' => [
                    'class' => 'time wed_pm'
                ],
            ])
            ->add('wed_am', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time wed_am'
                ],
            ])
            ->add('wed_am_end', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',

                'attr' => [
                    'class' => 'time wed_am'
                ],
            ])
            ->add('thu_pm', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time thu_pm'
                ],
            ])
            ->add('thu_pm_end', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',

                'attr' => [
                    'class' => 'time thu_pm'
                ],
            ])
            ->add('thu_am', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time thu_am'
                ],
            ])
            ->add('thu_am_end', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',

                'attr' => [
                    'class' => 'time thu_am'
                ],
            ])
            ->add('fri_pm', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time fri_pm'
                ],
            ])
            ->add('fri_pm_end', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',

                'attr' => [
                    'class' => 'time fri_pm'
                ],
            ])
            ->add('fri_am', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time fri_am'
                ],
            ])
            ->add('fri_am_end', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',

                'attr' => [
                    'class' => 'time fri_am'
                ],
            ])
            ->add('sat_pm', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time sat_pm'
                ],
            ])
            ->add('sat_pm_end', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',

                'attr' => [
                    'class' => 'time sat_pm'
                ],
            ])
            ->add('sat_am', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time sat_am'
                ],
            ])
            ->add('sat_am_end', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',

                'attr' => [
                    'class' => 'time sat_am'
                ],
            ])
            ->add('sun_pm', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time sun_pm'
                ],
            ])
            ->add('sun_pm_end', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',

                'attr' => [
                    'class' => 'time sun_pm'
                ],
            ])
            ->add('sun_am', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time sun_am'
                ],
            ])
            ->add('sun_am_end', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',

                'attr' => [
                    'class' => 'time sun_am'
                ],
            ])
            ->add('close_mon', ChoiceType::class, [
                'label' => 'Jour',
                'choices' => [
                    'Ouvert' => '',
                    'Matin' => 'close_pm',
                    'Après midi' => 'close_am',
                    'Journée' => 'close_day'
                ],
                'attr' => [
                    'class' => 'time time close_mon'
                ]
            ])
            ->add('close_tue', ChoiceType::class, [
                'label' => 'Jour',
                'choices' => [
                    'Ouvert' => '',
                    'Matin' => 'close_pm',
                    'Après midi' => 'close_am',
                    'Journée' => 'close_day'
                ],
                'attr' => [
                    'class' => 'time time close_tue'
                ]
            ])
            ->add('close_wed', ChoiceType::class, [
                'label' => 'Jour',
                'choices' => [
                    'Ouvert' => '',
                    'Matin' => 'close_pm',
                    'Après midi' => 'close_am',
                    'Journée' => 'close_day'
                ],
                'attr' => [
                    'class' => 'time time close_wed'
                ]
            ])
            ->add('close_thu', ChoiceType::class, [
                'label' => 'Jour',
                'choices' => [
                    'Ouvert' => '',
                    'Matin' => 'close_pm',
                    'Après midi' => 'close_am',
                    'Journée' => 'close_day'
                ],
                'attr' => [
                    'class' => 'time time close_thu'
                ]
            ])
            ->add('close_fri', ChoiceType::class, [
                'label' => 'Jour',
                'choices' => [
                    'Ouvert' => '',
                    'Matin' => 'close_pm',
                    'Après midi' => 'close_am',
                    'Journée' => 'close_day'
                ],
                'attr' => [
                    'class' => 'time time close_fri'
                ]
            ])
            ->add('close_sat', ChoiceType::class, [
                'label' => 'Jour',
                'choices' => [
                    'Ouvert' => '',
                    'Matin' => 'close_pm',
                    'Après midi' => 'close_am',
                    'Journée' => 'close_day'
                ],
                'attr' => [
                    'class' => 'time time close_sat'
                ]
            ])
            ->add('close_sun', ChoiceType::class, [
                'label' => 'Jour',
                'choices' => [
                    'Ouvert' => '',
                    'Matin' => 'close_pm',
                    'Après midi' => 'close_am',
                    'Journée' => 'close_day'
                ],
                'attr' => [
                    'class' => 'time time close_sun'
                ]
            ])
        ;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Schedule::class,

        ]);
    }
}
