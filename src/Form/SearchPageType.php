<?php

namespace App\Form;

use App\Entity\City;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SearchType
 * @package App\Form
 */
class SearchPageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('searchterm', SearchType::class, [
                'attr' => [
                    'class' => 'form-control form-control-lg not-dark',
                    'placeholder' => 'Votre recherche*'
                ]
            ])
            ->add('choice', ChoiceType::class, [
                'label' => 'Vous recherchez ?',
                'choices' => [
                    'Entreprise' => 1,
                    'Services' => 0
                ],
                'attr' => [
                    'class' => 'form-control form-control-lg not-dark'
                ]
            ])
            ->add('city', EntityType::class, [
                'class' => City::class,
                'label' => 'Ville',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.city', 'ASC');
                },
                'choice_label' => 'getCityPostal',
                'required' => false,
                'placeholder' => 'N\'importe quelle ville',
                'attr' => [
                    'class' => 'form-control form-control-lg not-dark selectpicker'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        ]);
    }
}
