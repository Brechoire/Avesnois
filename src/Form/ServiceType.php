<?php

namespace App\Form;

use App\Entity\Service;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

class ServiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Nom du service ou produit *'
            ])
            ->add('content', TextType::class, [
                'label' => 'Description avant *',
                'constraints' => [
                    new Length([
                        'max' => 150,
                        'maxMessage' => 'Désolé le texte est trop grand'
                    ])
                ]
            ])
            ->add('contentBack', TextType::class, [
                'label' => 'Description arrière *'
            ])
            ->add('description', CKEditorType::class, [
                'label' => 'Description complete',
                'config' => [
                    'toolbar' => "my_toolbar_2"
                ]
            ])
            ->add('colorBack', HiddenType::class, [
                'label' => 'Couleur de fond'
            ])
            ->add('imgService', FileType::class, [
                'label' => 'Image de fond *',
                'multiple' => false,
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new NotNull([
                       'message' => 'Image obligatoire'
                    ]),
                    new Image([
                        'maxSize' => '3M',
                        'maxSizeMessage' => "Le poid de l'image est supérieur à 3Mo"
                    ])
                ]
            ])
            ->add('tag', TextType::class, [
                'label' => 'Mot clé *'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Service::class,
        ]);
    }
}
