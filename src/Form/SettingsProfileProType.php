<?php

namespace App\Form;

use App\Entity\Page;
use App\Validators\CountService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SettingsProfileProType
 * @package App\Form
 */
class SettingsProfileProType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isPublished', CheckboxType::class, [
                'constraints' => [
                    new CountService()
                ]
            ])
            ->add('addressPublished', CheckboxType::class, [
            ])
            ->add('siretPublished', CheckboxType::class, [
            ])
            ->add('email1Published', CheckboxType::class, [
            ])
            ->add('tel1Published', CheckboxType::class, [
            ])
            ->add('schedulePublished', CheckboxType::class, [
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Page::class,
        ]);
    }
}
