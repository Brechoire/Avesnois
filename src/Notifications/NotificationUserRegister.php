<?php

namespace App\Notifications;

use Swift_Mailer;
use Swift_Message;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class NotificationUserRegister
 * @package App\Notifications
 */
class NotificationUserRegister
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $renderer;

    /**
     * NotificationUserRegister constructor.
     * @param Swift_Mailer $mailer
     * @param Environment $renderer
     */
    public function __construct(Swift_Mailer $mailer, Environment $renderer)
    {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
    }

    /**
     * @param $email
     * @param $username
     * @param $firstName
     * @param $lastName
     * @param $role
     * @param $city
     * @param $token
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function notify($email, $username, $firstName, $lastName, $role, $city, $token)
    {
        $message = (new Swift_Message('Merci pour votre inscription'))
            ->setFrom('contact@monavesnois.fr')
            ->setTo($email)
            ->setBody($this->renderer->render('emails/registration.html.twig', [
                'email' => $email,
                'username' => $username,
                'firstName' => $firstName,
                'lastName' => $lastName,
                'role' => $role,
                'city' => $city,
                'token' => $token
            ]), 'text/html');
        $this->mailer->send($message);
    }
}
