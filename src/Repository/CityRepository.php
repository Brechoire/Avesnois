<?php

namespace App\Repository;

use App\Entity\City;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method City|null find($id, $lockMode = null, $lockVersion = null)
 * @method City|null findOneBy(array $criteria, array $orderBy = null)
 * @method City[]    findAll()
 * @method City[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, City::class);
    }

    /**
     * @param $search
     * @return int|mixed|string
     */
    public function search($search)
    {
        $query = $this->createQueryBuilder('c');
        if ($search != null) {
            $query->where('MATCH_AGAINST(c.city) AGAINST(:searchterm boolean)>0')
                ->setParameter('searchterm', $search);
        }

        return $query->getQuery()->getResult();
    }
}
