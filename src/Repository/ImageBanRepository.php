<?php

namespace App\Repository;

use App\Entity\ImageBan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ImageBan|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImageBan|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImageBan[]    findAll()
 * @method ImageBan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageBanRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ImageBan::class);
    }

    /**
     * @param $id
     * @return int|mixed|string
     * @throws NonUniqueResultException
     */
    public function imageBanByPage($id)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.page = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
