<?php

namespace App\Repository;

use App\Entity\ImageLogo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ImageLogo|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImageLogo|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImageLogo[]    findAll()
 * @method ImageLogo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageLogoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ImageLogo::class);
    }

    /**
     * @param $id
     * @return int|mixed|string|null
     * @throws NonUniqueResultException
     */
    public function imageLogoByPage($id)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.page = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
