<?php

namespace App\Repository;

use App\Entity\NafCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NafCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method NafCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method NafCode[]    findAll()
 * @method NafCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NafCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NafCode::class);
    }

    // /**
    //  * @return NafCode[] Returns an array of NafCode objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NafCode
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
