<?php

namespace App\Repository;

use App\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findAll()
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    /**
     * @param $id
     * @return int|mixed|string|null
     */
    public function pageProfil($id)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.page = :id')
            ->leftJoin('p.social', 's', 's.page = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $search
     * @return int|mixed|string
     */
    public function search($search)
    {
        $query = $this->createQueryBuilder('p');
        if ($search != null) {
            $query
                ->where('p.is_activaded = 1')
                ->andWhere('p.valide_admin = 1')
                ->andWhere('p.name LIKE :searchterm')
                ->setParameter('searchterm', '%' . $search . '%');

            return $query->getQuery()->getResult();
        }
        return null;
    }
}
