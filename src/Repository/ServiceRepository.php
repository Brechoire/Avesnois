<?php

namespace App\Repository;

use App\Entity\Service;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Service|null find($id, $lockMode = null, $lockVersion = null)
 * @method Service|null findOneBy(array $criteria, array $orderBy = null)
 * @method Service[]    findAll()
 * @method Service[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Service::class);
    }

    /**
     * @param $id
     * @return Service|null
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function countServiceUser($id)
    {
        return $this->createQueryBuilder('s')
            ->select('count(s.id)')
            ->andWhere('s.page = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param $search
     * @return int|mixed|string
     */
    public function search($search)
    {
        $query = $this->createQueryBuilder('s');
        if ($search != null) {
            $query
                ->join('s.page', 'p')
                ->where('p.is_activaded = 1')
                ->andWhere('p.valide_admin = 1')
                ->andWhere('s.title LIKE :searchterm')
                ->setParameter('searchterm', '%' . $search . '%');

            return $query->getQuery()->getResult();
        }
        return null;
    }
}
