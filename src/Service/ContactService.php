<?php

namespace App\Service;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;

class ContactService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * ContactService constructor.
     * @param EntityManagerInterface $entityManager
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $formFactory)
    {
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;
    }

    /**
     * Passe à true le message
     * @param int $id
     */
    public function updateView(int $id)
    {
        $contact = $this->entityManager->getRepository('App:ContactPro')->find($id);

        if ($contact->getView() == 0) {
            $contact->setView(1);
            $contact->setViewAt(new DateTime('now'));
            $this->entityManager->persist($contact);
            $this->entityManager->flush();
        }
    }
}
