<?php

namespace App\Service;

use App\Entity\ContactPro;
use App\Entity\ImageBan;
use App\Entity\ImageLogo;
use App\Entity\Page;
use App\Entity\Schedule;
use App\Entity\Service;
use App\Entity\Social;
use App\EventListener\RedirectExceptionListener;
use App\Exception\RedirectException;
use App\Form\ContactProType;
use App\Form\EditPageType;
use App\Form\PageSocialNetworksType;
use App\Form\PageType;
use App\Form\ScheduleType;
use App\Form\ServiceType;
use App\Form\SettingsProfileProType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class PageProService
 * @package App\Service\Page
 */
class PageProService
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    /**
     * @var FormFactoryInterface
     */
    private FormFactoryInterface $formFactory;
    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $tokenStorage;
    /**
     * @var ParameterBagInterface
     */
    private ParameterBagInterface $parameterBag;
    /**
     * @var RouterInterface
     */
    private RouterInterface $router;
    /**
     * @var RedirectExceptionListener
     */
    private RedirectExceptionListener $redirect;
    /**
     * @var FlashBagInterface
     */
    private FlashBagInterface $flashBag;

    /**
     * PageProService constructor.
     * @param EntityManagerInterface $entityManager
     * @param FormFactoryInterface $formFactory
     * @param TokenStorageInterface $tokenStorage
     * @param ParameterBagInterface $parameterBag
     * @param RouterInterface $router
     * @param RedirectExceptionListener $redirect
     * @param FlashBagInterface $flashBag
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        FormFactoryInterface $formFactory,
        TokenStorageInterface $tokenStorage,
        ParameterBagInterface $parameterBag,
        RouterInterface $router,
        RedirectExceptionListener $redirect,
        FlashBagInterface $flashBag
    ) {
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;
        $this->tokenStorage = $tokenStorage;
        $this->parameterBag = $parameterBag;
        $this->router = $router;
        $this->redirect = $redirect;
        $this->flashBag = $flashBag;
    }

    /**
     * Check id de l'user
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->tokenStorage->getToken()->getUser()->getId();
    }

    /**
     * @param Request $request
     * @return FormInterface
     */
    public function addPagePro(Request $request): FormInterface
    {
        $page = new Page();

        $form = $this->formFactory->create(PageType::class, $page);

        $form->handleRequest($request);
        $user = $this->tokenStorage->getToken()->getUser();

        if ($form->isSubmitted() && $form->isValid()) {
            $page->setEmail1Published(1);
            $page->setTel1Published(1);
            $page->setAddressPublished(1);
            $page->setSiretPublished(1);

            $page->setUser($user);
            $this->entityManager->persist($page);
            $this->entityManager->flush();

            $social = new Social();
            $social->setPage($page);

            $this->entityManager->persist($social);
            $this->entityManager->flush();

            $schedule = new Schedule();
            $schedule->setPage($page);

            $this->entityManager->persist($schedule);
            $this->entityManager->flush();
        }

        return $form;
    }

    /**
     * Vérifie si un utilisateur a une page pro ou non
     * @param $id
     * @return Page|object|null
     */
    public function pagePro($id)
    {
        return $this->entityManager->getRepository(Page::class)->findOneBy(['user' => $id]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return FormInterface
     */
    public function editPage(Request $request, $id): FormInterface
    {
        $id = $this->tokenStorage->getToken()->getUser();

        $page = $this->entityManager->getRepository(Page::class)->findOneBy(array('user' => $id));

        $form = $this->formFactory->create(EditPageType::class, $page);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $logo = $form->get('imageLogo')->getData();
            $ban = $form->get('imageBan')->getData();

            $this->uploadLogo($logo, $page);
            $this->uploadBan($ban, $page);

            $this->entityManager->persist($page);
            $this->entityManager->flush();
        }

        return $form;
    }

    /**
     * @param $logo | Data du Logo
     * @param $page | Référence à la page
     */
    public function uploadLogo($logo, $page)
    {
        if ($logo != null) {
            $img_repo =  $this->entityManager->getRepository(ImageLogo::class)->findOneBy(array('page' => $page));

            $imgLogo = md5(uniqid()) . '.' . $logo->guessExtension();

            $logo->move(
                $this->parameterBag->get('images_directory_logo'),
                $imgLogo
            );

            if ($img_repo != null) {
                unlink(
                    $this->parameterBag->get('images_directory_logo') . '/' .
                    $img_repo->getName()
                );
                $img_repo->setName($imgLogo);
                $img_repo->setIdPage($page);
                $this->entityManager->persist($img_repo);
            } else {
                $img = new ImageLogo();
                $img->setName($imgLogo);
                $img->setIdPage($page);
                $this->entityManager->persist($img);
            }
        }
    }

    /**
     * @param $ban
     * @param $page
     */
    public function uploadBan($ban, $page)
    {
        if ($ban != null) {
            $img_repo =  $this->entityManager->getRepository(ImageBan::class)->findOneBy(array('page' => $page));

            $imgBan = md5(uniqid()) . '.' . $ban->guessExtension();

            $ban->move(
                $this->parameterBag->get('images_directory_ban'),
                $imgBan
            );

            if ($img_repo != null) {
                unlink(
                    $this->parameterBag->get('images_directory_ban') . '/' .
                    $img_repo->getName()
                );
                $img_repo->setName($imgBan);
                $img_repo->setIdPage($page);
                $this->entityManager->persist($img_repo);
            } else {
                $img = new ImageBan();
                $img->setName($imgBan);
                $img->setIdPage($page);
                $this->entityManager->persist($img);
            }
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return FormInterface
     */
    public function editPageSocialNetworks(Request $request, $id): FormInterface
    {
        $page = $this->entityManager->getRepository(Social::class)->findOneBy(array('page' => $id));

        $form = $this->formFactory->create(PageSocialNetworksType::class, $page);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($page);
            $this->entityManager->flush();
        }

        return $form;
    }

    /**
     * @param Request $request
     * @param $id
     * @return FormInterface
     */
    public function settingsPage(Request $request, $id): FormInterface
    {
        $id = $this->tokenStorage->getToken()->getUser();

        $page = $this->entityManager->getRepository(Page::class)->findOneBy(array('user' => $id));

        $form = $this->formFactory->create(SettingsProfileProType::class, $page);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($page);
            $this->entityManager->flush();
        }

        return $form;
    }

    /**
     * @param Request $request
     * @param $id
     * @return FormInterface
     */
    public function schedule(Request $request, $id): FormInterface
    {
        $page = $this->entityManager->getRepository(Schedule::class)->findOneBy(array('page' => $id));
        $form = $this->formFactory->create(ScheduleType::class, $page);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($page);
            $this->entityManager->flush();
        }

        return $form;
    }

    /**
     * @param Request $request
     * @param $id
     * @return FormInterface
     */
    public function addService(Request $request, $id): FormInterface
    {
        $service = new Service();

        $form = $this->formFactory->create(ServiceType::class, $service);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $img = $form->get('imgService')->getData();

            $imgLogo = md5(uniqid()) . '.' . $img->guessExtension();

            $img->move(
                $this->parameterBag->get('images_directory_service'),
                $imgLogo
            );

            $service->setPage($id);
            $service->setImgService($imgLogo);
            $this->entityManager->persist($service);
            $this->entityManager->flush();
        }

        return $form;
    }

    /**
     * @param Request $request
     * @param $id
     * @param $idpage
     * @return FormInterface
     */
    public function editService(Request $request, $id, $idpage): FormInterface
    {
        $page = $this->entityManager->getRepository(Service::class)->findOneBy(array('id' => $id, 'page' => $idpage));

        $form = $this->formFactory->create(ServiceType::class, $page);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $img = $form->get('imgService')->getData();

            if ($img != null) {
                $imgLogo = md5(uniqid()) . '.' . $img->guessExtension();

                $img->move(
                    $this->parameterBag->get('images_directory_service'),
                    $imgLogo
                );

                $page->setImgService($imgLogo);
            }


            $this->entityManager->persist($page);
            $this->entityManager->flush();
        }

        return $form;
    }

    /**
     * Liste des services
     * @param $id
     * @return Service[]|object[]
     */
    public function listService($id): array
    {
        return $this->entityManager->getRepository(Service::class)->findBy(['page' => $id]);
    }

    /**
     * Vérifie si l'utilisateur peut modifier le service.
     * @param $id | id du service
     * @param $idPage | id de la page
     * @return Service|object|null
     */
    public function checkService($id, $idPage)
    {
        return $this->entityManager->getRepository(Service::class)->findBy(array('id' => $id, 'page' => $idPage));
    }

    /**
     * Compte le nombre de service d'une page pro
     * @param $id | id de la page
     * @return mixed
     */
    public function countServiceUser($id)
    {
        return $this->entityManager->getRepository(Service::class)->countServiceUser($id);
    }

    /**
     * Récupère les données de la page via le slug
     * @param $slug | Slug
     * @return Page|object|null
     */
    public function showPagePro($slug)
    {
        return $this->entityManager->getRepository(Page::class)->findOneBy(['slug' => $slug]);
    }

    /**
     * Redirige l'utilisateur si la page n'est pas activé ou publié
     * @param $published | Page publié
     * @param $activaded | Page activé
     * @throws RedirectException
     */
    public function redirectPage($published, $activaded)
    {
        if ($published == false && $activaded == false) {
            $this->flashBag->add('warning', 'La page que vous demandez n\'est pas activé');
            throw new RedirectException($this->router->generate('home'));
        } elseif ($published == true && $activaded == false) {
            $this->flashBag->add(
                'warning',
                'La page que vous demandez est publié par l\'utilisateur mais n\'est pas validé par Mon Avesnois'
            );
            throw new RedirectException($this->router->generate('home'));
        } elseif ($published == false && $activaded == true) {
            $this->flashBag->add(
                'warning',
                'La page que vous demandez n\'est pas publié mais est validé par Mon Avesnois'
            );
            throw new RedirectException($this->router->generate('home'));
        }
    }

    /**
     * Vérifie si la page pro demandé existe
     * @param $page | Objet Page
     * @throws RedirectException
     */
    public function existPage($page)
    {
        if ($page == false) {
            $this->flashBag->add('warning', 'La page que vous demandez n\'existe pas');
            throw new RedirectException($this->router->generate('home'));
        }
    }

    /**
     * @param Request $request
     * @param $slug
     * @return FormInterface
     */
    public function contact(Request $request, $slug): FormInterface
    {
        $contact = new ContactPro();

        $form = $this->formFactory->create(ContactProType::class, $contact);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact->setPage($slug);
            $contact->setUser($this->tokenStorage->getToken()->getUser());
            $this->entityManager->persist($contact);
            $this->entityManager->flush();
        }

        return $form;
    }

    /**
     * @param $id
     * @return array
     */
    public function listContact($id): array
    {
        return $this->entityManager->getRepository(ContactPro::class)->findBy(
            ['page' => $id],
            ['created_at' => 'DESC']
        );
    }
}
