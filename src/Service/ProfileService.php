<?php

namespace App\Service;

use App\Entity\ConnectionHistory;
use App\Entity\User;
use App\EventListener\RedirectExceptionListener;
use App\Exception\RedirectException;
use App\Form\EditPasswordProfileType;
use App\Form\EditProfileUserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface as FormInterfaceAlias;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProfileService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var RedirectExceptionListener
     */
    private $redirect;
    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * EditPasswordProfileService constructor.
     * @param EntityManagerInterface $entityManager
     * @param FormFactoryInterface $formFactory
     * @param TokenStorageInterface $tokenStorage
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param RouterInterface $router
     * @param RedirectExceptionListener $redirect
     * @param FlashBagInterface $flashBag
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        FormFactoryInterface $formFactory,
        TokenStorageInterface $tokenStorage,
        UserPasswordEncoderInterface $passwordEncoder,
        RouterInterface $router,
        RedirectExceptionListener $redirect,
        FlashBagInterface $flashBag
    ) {
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;
        $this->tokenStorage = $tokenStorage;
        $this->passwordEncoder = $passwordEncoder;
        $this->router = $router;
        $this->redirect = $redirect;
        $this->flashBag = $flashBag;
    }

    /**
     * Modification des informations de l'utilisateur
     * @param Request $request
     * @return FormInterfaceAlias
     */
    public function editProfileUser(Request $request): FormInterfaceAlias
    {
        $user = $this->tokenStorage->getToken()->getUser();

        $form = $this->formFactory->create(EditProfileUserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();
        }

        return $form;
    }

    /**
     * Modification du mot de passe
     * @param Request $request
     * @return FormInterfaceAlias
     */
    public function editPassword(Request $request): FormInterfaceAlias
    {
        $id = $this->tokenStorage->getToken()->getUser();

        $user = $this->entityManager->getRepository(User::class)->findOneBy(array('id' => $id));

        $form = $this->formFactory->create(EditPasswordProfileType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );
            $this->entityManager->flush();
        }

        return $form;
    }

    /**
     * Suppression de l'utilisateur
     * @param Request $request
     */
    public function deleteUser(Request $request)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $this->entityManager->remove($user);
        $this->entityManager->flush();
        $this->tokenStorage->setToken(null);
        $request->getSession()->invalidate();
    }

    /**
     * Liste les 5 dernières informations de connexion
     * @param $id
     * @return ConnectionHistory[]|object[]
     */
    public function connectionHistory($id): array
    {
        return $this->entityManager->getRepository(ConnectionHistory::class)->findBy(
            ['user' => $id],
            ['last_login' => 'desc'],
            5
        );
    }

    /**
     * Liste les informations de connexion
     * @param $id
     * @return ConnectionHistory[]|object[]
     */
    public function connectionHistoryPro($id): array
    {
        return $this->entityManager->getRepository(ConnectionHistory::class)->findBy(
            ['user' => $id],
            ['last_login' => 'DESC']
        );
    }

    /**
     * Check id de l'user
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->tokenStorage->getToken()->getUser()->getId();
    }

    /**
     * @param $id |Id de l'utilisateur
     * @param $idUserRequest
     * @throws RedirectException
     */
    public function checkGetIdUser($id, $idUserRequest)
    {
        if (!$id or $id != $idUserRequest) {
            throw new RedirectException($this->router->generate('profile', ['id' => $id]));
        }
    }
}
