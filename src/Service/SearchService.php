<?php

namespace App\Service;

use App\Entity\Page;
use App\Entity\Service;
use App\Form\SearchPageType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SearchService
 * @package App\Service
 */
class SearchService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * SearchService constructor.
     * @param EntityManagerInterface $entityManager
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(EntityManagerInterface $entityManager, FormFactoryInterface $formFactory)
    {
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;
    }

    /**
     * @param Request $request|Request
     * @return array
     */
    public function search(Request $request): array
    {
        $page = null;
        $services = null;
        $city = null;
        $choice = null;
        $search = null;

        $form = $this->formFactory->create(SearchPageType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $choice = $form->get('choice')->getData();
            $city = $form->get('city')->getData();
            $page = $this->entityManager->getRepository(Page::class)->search($form->get('searchterm')->getData());
            $services = $this->entityManager->getRepository(
                Service::class
            )
                ->search($form->get('searchterm')->getData());
            $search = $form->get('searchterm')->getData();
        }

        return  [
            'form' => $form,
            'pages' => $page,
            'choice' => $choice,
            'services' => $services,
            'city' => $city,
            'search' => $search
        ];
    }
}
