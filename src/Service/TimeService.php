<?php

namespace App\Service;

use DateTime;
use Exception as ExceptionAlias;

/**
 * Class TimeService
 * @package App\Service
 */
class TimeService
{
    /**
     * @param $date
     * @return string
     * @throws ExceptionAlias
     */
    public function getRelativeTime($date): string
    {
        try {
            $date_a_comparer = new DateTime($date);
        } catch (ExceptionAlias $e) {
        }
        $date_actuelle = new DateTime("now");
        $intervalle = $date_a_comparer->diff($date_actuelle);

        if ($date_a_comparer > $date_actuelle) {
            $prefixe = 'dans ';
        } else {
            $prefixe = 'il y a ';
        }

        $ans = $intervalle->format('%y');
        $mois = $intervalle->format('%m');
        $jours = $intervalle->format('%d');
        $heures = $intervalle->format('%h');
        $minutes = $intervalle->format('%i');
//        $secondes = $intervalle->format('%s');

        if ($ans != 0) {
            $relative_date = $prefixe . $ans . ' an' . (($ans > 1) ? 's' : '');
            if ($mois >= 6) {
                $relative_date .= ' et demi';
            }
        } elseif ($mois != 0) {
            $relative_date = $prefixe . $mois . ' mois';
            if ($jours >= 15) {
                $relative_date .= ' et demi';
            }
        } elseif ($jours != 0) {
            $relative_date = $prefixe . $jours . ' jour' . (($jours > 1) ? 's' : '');
        } elseif ($heures != 0) {
            $relative_date = $prefixe . $heures . ' heure' . (($heures > 1) ? 's' : '');
        } elseif ($minutes != 0) {
            $relative_date = $prefixe . $minutes . ' minute' . (($minutes > 1) ? 's' : '');
        } else {
            $relative_date = $prefixe . ' quelques secondes';
        }

        return $relative_date;
    }
}
