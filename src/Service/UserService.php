<?php

namespace App\Service;

use App\Entity\User;
use App\Form\RegisterType;
use App\Form\RegistrationFormType;
use App\Notifications\NotificationUserProRegister;
use App\Notifications\NotificationUserRegister;
use App\Security\UserAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Swift_Mailer;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class UserService
 * @package App\Service
 */
class UserService
{
    /**
     * @var EntityManagerInterface
     */
    private $doctrine;
/**
     * @var FormFactoryInterface
     */
    private $form;
/**
     * @var UserPasswordEncoderInterface
     */
    private $passEncoder;
/**
     * @var AuthenticationUtils
     */
    private $loginUtils;
/**
     * @var AuthorizationCheckerInterface
     */
    private $secuCheck;
/**
     * @var TokenStorageInterface
     */
    private $security;
/**
     * @var Swift_Mailer
     */
    private $mailer;
/**
     * @var Environment
     */
    private $template;
/**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;
/**
     * @var NotificationUserRegister
     */
    private $notify;
/**
     * @var GuardAuthenticatorHandler
     */
    private $guardHandler;
/**
     * @var UserAuthenticator
     */
    private $authenticator;
/**
     * @var NotificationUserProRegister
     */
    private $notifyPro;

    /**
     * UserService constructor.
     * @param EntityManagerInterface $doctrine
     * @param FormFactoryInterface $form
     * @param UserPasswordEncoderInterface $passEncoder
     * @param AuthenticationUtils $loginUtils
     * @param AuthorizationCheckerInterface $secuCheck
     * @param TokenStorageInterface $security
     * @param Swift_Mailer $mailer
     * @param Environment $template
     * @param TokenGeneratorInterface $tokenGenerator
     * @param NotificationUserRegister $notify
     * @param GuardAuthenticatorHandler $guardHandler
     * @param UserAuthenticator $authenticator
     * @param NotificationUserProRegister $notifyPro
     */
    public function __construct(
        EntityManagerInterface $doctrine,
        FormFactoryInterface $form,
        UserPasswordEncoderInterface $passEncoder,
        AuthenticationUtils $loginUtils,
        AuthorizationCheckerInterface $secuCheck,
        TokenStorageInterface $security,
        Swift_Mailer $mailer,
        Environment $template,
        TokenGeneratorInterface $tokenGenerator,
        NotificationUserRegister $notify,
        GuardAuthenticatorHandler $guardHandler,
        UserAuthenticator $authenticator,
        NotificationUserProRegister $notifyPro
    ) {
        $this->doctrine = $doctrine;
        $this->form = $form;
        $this->passEncoder = $passEncoder;
        $this->loginUtils = $loginUtils;
        $this->secuCheck = $secuCheck;
        $this->security = $security;
        $this->mailer = $mailer;
        $this->template = $template;
        $this->tokenGenerator = $tokenGenerator;
        $this->notify = $notify;
        $this->guardHandler = $guardHandler;
        $this->authenticator = $authenticator;
        $this->notifyPro = $notifyPro;
    }

    /**
     * @param Request $request
     * @return FormInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function registerUser(Request $request): FormInterface
    {
        $count = $this->doctrine->getRepository(User::class)->findAll();
        $user = new User();
        $form = $this->form->create(RegistrationFormType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $token = substr($form->get('username')->getData(), 0, 3)
                . rand(15, 629)
                . '-'
                . rand(300, 9000)
                . '-' . substr($this->tokenGenerator->generateToken(), 0, 5);
            if (count($count) == "") {
                $user->setRoles(['ROLE_ADMIN']);
            } else {
                $user->setRoles(['ROLE_USER']);
            }

            $role = $form->get('role')->getData();
            $user->setRoles([$role]);
            $user->setToken($token);
            $user->setCreatedIp($_SERVER['REMOTE_ADDR']);
            $user->setHostName($host = php_uname('n'));
            $password = $this->passEncoder->encodePassword($user, $user->getPassword());
            if ($form->get('role')->getData() == "ROLE_PRO") {
                $this->notifyPro->notify(
                    $form->get('email')->getData(),
                    $form->get('username')->getData(),
                    $form->get('firstName')->getData(),
                    $form->get('lastName')->getData(),
                    $form->get('role')->getData(),
                    $form->get('city')->getData(),
                    $token
                );
            } else {
                $this->notify->notify(
                    $form->get('email')->getData(),
                    $form->get('username')->getData(),
                    $form->get('firstName')->getData(),
                    $form->get('lastName')->getData(),
                    $form->get('role')->getData(),
                    $form->get('city')->getData(),
                    $token
                );
            }

            $user->setPassword($password);
//            $this->doctrine->persist($user);
//            $this->doctrine->flush();

            $this->guardHandler->authenticateUserAndHandleSuccess($user, $request, $this->authenticator, 'main');
        }

        return $form;
    }

    /**
     * Checks if user and token match
     * @param $username
     * @param $tokenkey
     * @return mixed
     */
    public function valideRegister($username, $tokenkey)
    {
        $valide = $this->doctrine->getRepository(User::class)->checkValideUserRegister($username, $tokenkey);
        return $valide;
    }

    /**
     * Update the field to true
     * @param $username
     * @return mixed
     */
    public function updateValideUser($username)
    {
        return $this->doctrine->getRepository(User::class)->UpdateValide($username);
    }

    /**
     * Checks if the user is already active
     * @param $username
     * @return mixed
     */
    public function userCheckActive($username)
    {
        return $this->doctrine->getRepository(User::class)->UserCheckActive($username);
    }

    /**
     * Checks if the user is ban
     * @param $username
     * @return mixed
     */
    public function userCheckBanned($username)
    {
        return $this->doctrine->getRepository(User::class)->UserCheckBanned($username);
    }

    /**
     * Count Users
     * @return mixed
     */
    public function countUser()
    {
        return $this->doctrine->getRepository(User::class)->countUser();
    }
}
