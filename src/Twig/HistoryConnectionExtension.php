<?php

namespace App\Twig;

use App\Entity\ConnectionHistory;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Récupération des derniers infos de connexion de l'utilisateur
 * Class HistoryConnectionExtension
 * @package App\Twig
 */
class HistoryConnectionExtension extends AbstractExtension
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AppExtension constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('history_connection', [$this, 'history_connection']),
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function historyConnection($id): array
    {
        return $this->entityManager->getRepository(ConnectionHistory::class)->findBy(
            ['user' => $id],
            ['last_login' => 'desc'],
            5
        );
    }
}
