<?php

namespace App\Twig;

use App\Entity\ContactPro;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Récupération du nombre de message non lu
 * Class MessageCountExtension
 * @package App\Twig
 */
class MessageCountExtension extends AbstractExtension
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * AppExtension constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('message_count', [$this, 'getMessageCount']),
        ];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getMessageCount($id)
    {
        return $this->entityManager->getRepository(ContactPro::class)->messageCount($id);
    }
}
