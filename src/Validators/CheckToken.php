<?php

namespace App\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * Class CheckToken
 * @package App\Validators
 */
class CheckToken extends Constraint
{
    public $message = 'Désolé le code n\'est pas valide. Vérifier votre email d\'inscription';

    public function validatedBy(): string
    {
        return get_class($this) . 'Validator';
    }
}
