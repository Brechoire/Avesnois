<?php

namespace App\Validators;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class CheckTokenValidator
 * @package App\Validators
 */
class CheckTokenValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $doctrine;

    /**
     * CountTicketValidator constructor.
     * @param EntityManagerInterface $doctrine
     */
    public function __construct(EntityManagerInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$value) {
            return;
        }

        $token = $this->doctrine
            ->getRepository(User::class)
            ->findBy(array('token' => $value))
        ;

        if ($token == null) {
            $this->context->addViolation($constraint->message);
        }
    }
}
