<?php

namespace App\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * Vérifie si l'utilisateur à 5 services
 * Class CountService
 * @package App\Validators
 */
class CountService extends Constraint
{
    public $message = 'Désolé vous devez ajouter 5 services pour pouvoir publier votre page';

    public function validatedBy(): string
    {
        return get_class($this) . 'Validator';
    }
}
