<?php

namespace App\Validators;

use App\Entity\Page;
use App\Entity\Service;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class CountServiceValidator
 * @package App\Validators
 */
class CountServiceValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
/**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
/**
     * CountServiceValidator constructor.
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$value) {
            return;
        }
        $page = $this->em->getRepository(Page::class)->findBy(['user' => $this->tokenStorage->getToken()->getUser()]);
        $service = $this->em
            ->getRepository(Service::class)
            ->countServiceUser($page);
        if ($service != 5) {
            $this->context->addViolation($constraint->message);
        }
    }
}
