<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class HomeControllerTest extends WebTestCase
{
    public function testHomeController()
    {
        $client = static::createClient();
        $client->request('GET', '/testcode');
        $this->assertResponseStatusCodeSame(404);
    }
}
